<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],


         "db" => [
            "host" => "www.soyeducadorasereyd.com",
            "dbname" => "soyeduca_sereyd",
            "user" => "soyeduca_lconder",
            "pass" => "0112358def"
        ],
    ],
];
