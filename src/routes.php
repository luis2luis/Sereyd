<?php
// Routes

require __DIR__ . '/../routes/index.php';
require __DIR__ . '/../routes/user.php';
require __DIR__ . '/../routes/login.php';
require __DIR__ . '/../routes/group.php';
require __DIR__ . '/../routes/student.php';
require __DIR__ . '/../routes/homework.php';
require __DIR__ . '/../routes/cooperation.php';
require __DIR__ . '/../routes/payment.php';
require __DIR__ . '/../routes/planification.php';
require __DIR__ . '/../routes/diagnostic.php';
require __DIR__ . '/../routes/group_diagnostic.php';
require __DIR__ . '/../routes/group_evaluation.php';
require __DIR__ . '/../routes/planification_user.php';
require __DIR__ . '/../routes/test.php';
require __DIR__ . '/../routes/question_student.php';
require __DIR__ . '/../routes/day_planning.php';
require __DIR__ . '/../routes/resource.php';
require __DIR__ . '/../routes/resource_user.php';
require __DIR__ . '/../routes/prueba.php';
require __DIR__ . '/../routes/evaluation.php';
require __DIR__ . '/../routes/password.php';
require __DIR__ . '/../routes/utils.php';
require __DIR__ . '/../routes/openpay.php';
require __DIR__ . '/../routes/diary.php';
require __DIR__ . '/../routes/discussion.php';
require __DIR__ . '/../routes/answer.php';
require __DIR__ . '/../routes/answer_ranking.php';
require __DIR__ . '/../routes/subscription.php';
require __DIR__ . '/../routes/dashboard.php';
