<?php

$app->get('/resource_user/[{id_user}]', function($request, $response, $args){

	$data = array();
	$http_response = 200;
	$id_user = $args["id_user"];
	$sql = "SELECT r.id_resource as id_resource_file, r.name, r.url FROM user_resource ur INNER JOIN resource r ON ur.id_resource=r.id_resource WHERE id_user=:id_user";
	$sth = $this->db->prepare($sql);
	try {
		$sth->bindParam("id_user", $id_user);
		$sth->execute();
		$resources =  $sth->fetchAll();
		if($resources != null){
			$resourcs = [];
			foreach ($resources as $key) {
				$key["name"] = utf8_encode($key["name"]);
				$resourcs[] = $key;
			}
			$data["error"] = 0;
			$data["resources"] = $resourcs;
		}else{
			$data["error"] = 1;
			$http_response = 500;
		}
	} catch (PDOException $e) {
		$data["error"] = 1;
		$data["description"] = $e->getMessage();
		$http_response = 500;
	}
	return $this->response->withJson($data, $http_response);

});

$app->post('/resource_user/', function ($request, $response) {

	$data = array();
	$http_response = 200;
	$input = $request->getParsedBody();
	$c = checkPublicLibrary($input['id_resource'], $this->db);

	
	$sql = "INSERT INTO `user_resource` (id_user, id_resource) VALUES (:id_user, :id_resource)";
	$sth = $this->db->prepare($sql);
	try{
		$sth->bindParam("id_user", $input['id_user']);
		$sth->bindParam("id_resource", $input['id_resource']);
		$pay 	= checkPaymentB($input['id_user'], $this->db);
		$count 	= checkKeys($input['id_user'],$input['id_resource'], $this->db);
	
		if($count["error"]==1){
			$data["error"] 			= 2;
			$data["count"] 			= intval($count["count"]);
			$http_response 			= 200;
			$data["description"] 	= "Ya tienes este recurso";
		}else{
			if($pay["error"] == 0 && $pay["payments"] > 0){
				$sth->execute();
				$data["error"] = 0;
				$data["id_category_resource"] = $c["id_category_resource"];
				$data["description"] = "Recurso asignado con éxito";
				if($c["id_category_resource"]!=3)
					updatePaymentB($input['id_user'], $this->db);
				$data["resource_count"] = getCountPaymentB($input['id_user'], $this->db);
			}else{
				$data["error"] = 1;
				$http_response = 500;
				if($pay["payments"] == 0)
					$data["description"] = "Ya no cuentas con recursos disponibles";
				else
					$data["description"] = "Error al asignar recurso";
				$data["resource_count"] = $pay["payments"];
			}
		}
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = "Error: ".$e->getMessage();
	}


	return $this->response->withJson($data, $http_response);

});


function updatePaymentB($id_user, $db){

	$sql = "UPDATE payment SET resource_count=resource_count-1 WHERE id_user = :id_user";
	$r = array();
	$sth = $db->prepare($sql);
	try{		
		$sth->bindParam("id_user", $id_user);
		$band = $sth->execute();
		if($band!=false){
			$r["error"] = 0;
		}else{
			$r["error"] = 1;
		}
		return $r;

	}catch(PDOException $e){

		$r["error"] = 1;
		$r["description"] = "Error al modificar el pago ". $e->getMessage();
		return $r;
	}

}


function checkPaymentB($id_user, $db){

	$sql = "SELECT SUM(resource_count) as resource_count FROM payment WHERE id_user=:id_user";
	$r = array();
	$sth = $db->prepare($sql);
	try{

		$sth->bindParam("id_user", $id_user);
		$sth->execute();
		$payments = $sth->fetchObject();
		if($payments->resource_count == null){
			$r["error"] 	= 1;
			$r["payments"] 	= 0;
		}else{
			$r["error"] 	= 0;
			$r["payments"] 	= $payments->resource_count;
		}
		return $r;

	}catch(PDOException $e){

		$r["error"] = 1;
		$r["description"] = "Error obteniendo los pagos: ".$e->getMessage();
		return $r;
	}
	
}

function getCountPaymentB($id_user, $db){

	$sql = "SELECT resource_count FROM payment WHERE id_user=:id_user";
	$r = array();
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_user", $id_user);
		$sth->execute();
		$payments = $sth->fetchObject();
		if($payments->resource_count == null){
			$r["error"] = 1;
			$r["payments"] = -1;
		}else{
			$r["error"] = 0;
			$r["payments"] = $payments->resource_count;
		}
		return $r;

	}catch(PDOException $e){

		$r["error"] = 1;
		$r["description"] = "Error obteniendo el contador de pagos ".$e->getMessage();
		return $r;
	}
	
}


function checkKeys($id_user, $id_resource, $db){

	$sql = "SELECT COUNT(*) as count FROM user_resource WHERE id_user=:id_user AND id_resource=:id_resource";
	$r = array();
	try {
		$sth = $db->prepare($sql);
		$sth->bindParam("id_user", $id_user);
		$sth->bindParam("id_resource", $id_resource);
		$sth->execute();
		$count = $sth->fetchObject();
		if($count->count > 0){
			$r["error"] = 1;
			$r["count"] = $count->count;
		}else{
			$r["error"] = 0;
			$r["count"] = $count->count;
		}
	} catch (PDOException $e) {
		$r["error"] = 1;
	}
	return $r;
}

function checkPublicLibrary($id_resource, $db){

	$sql = "SELECT id_category_resource FROM resource WHERE id_resource=:id_resource";
	$r = array();
	try {
		$sth = $db->prepare($sql);
		$sth->bindParam("id_resource", $id_resource);
		$sth->execute();
		$object = $sth->fetchObject();
		$r["error"] = 0;
		$r["id_category_resource"] = intval($object->id_category_resource);
	} catch (PDOException $e) {
		$r["error"] = 1;
	}
	return $r;

}

