<?php

require __DIR__ . '/../lib/Openpay/Openpay.php';

//define("MERCHANT_ID", 'm0nccdfwzr0pf8mdiuhd'); //PRUEBAS
//define("PRIVATE_KEY", 'sk_dcbbdbcc3d564392926d3fd92ce4b6e3'); //PRUEBAS

define("MERCHANT_ID", 'mbljemtdh8osybgrddpt'); //PRODUCCION
define("PRIVATE_KEY", 'sk_43d8e50871cd40a085e3b38a437a7933'); //PRODUCCION


//define("PLAN_MENSUAL", 'przvyau5hlm3q9xihmoi'); //PRUEBAS
//define("PLAN_ANNUAL", 'p1uzlxjveyrrehvpjyfy'); //PRUEBAS

define("PLAN_MENSUAL", 'pmhdobm6wxln7arypbpe'); //PRODUCCION
define("PLAN_ANNUAL", 'pmzifpjyv7hxdii1w7tn'); //PRODUCCION

date_default_timezone_set('America/Mexico_City');

$app->post('/openpay/[{id_user}]', function ($request, $response, $args) {


	$http_response	= 500;
	$id_user				= $args["id_user"];
	$fecha 					= date("Y-m-d H:i:s");
	$data 					= array();
	$input 					= $request->getParsedBody();
	$data_plan			= array();

	$token_id						=	$input["token_id"];
	$device_session_id	=	$input["device_session_id"];//producción
	//$device_session_id	=	$input["deviceIdHiddenFieldName"]; //pruebas


	try{

		Openpay::setId(MERCHANT_ID);
		Openpay::setApiKey(PRIVATE_KEY);
		$openpay 		= Openpay::getInstance(MERCHANT_ID, PRIVATE_KEY);
		Openpay::setProductionMode(true); //producción

		$user_data 				= getDataCostumer($id_user, $this->db);
		$id_openpay_user	= $user_data->id_openpay;
		$name_user				= $user_data->name;
		$email_user				= $user_data->email;

		//1 para anual
		//0 para mensual
		$type 		= intval($input["type"]);
		switch ($type) {
			case 0:
				$temp = getDataPlan(PLAN_MENSUAL, $this->db);
				$data_plan["id"]			=	PLAN_MENSUAL;
				$data_plan["name"]		=	$temp->name;
				$data_plan["amount"]	=	$temp->amount;
				break;
			case 1:
				$temp =	getDataPlan(PLAN_ANNUAL, $this->db);
				$data_plan["id"]			=	PLAN_ANNUAL;
				$data_plan["name"]		=	$temp->name;
				$data_plan["amount"]	=	$temp->amount;
				break;
			default:
				break;
		}

		if(strlen($id_openpay_user)<=0) {
			error_log( "no hay id de openpay se da de alta al user ");
			$id_openpay		 		= createUserOpenpay($name_user, $email_user, $id_user, $openpay, $this->db);
			error_log("Creación de usuario en Openpay: ".$id_openpay);
			$id_card 					= addCardToUser($id_openpay, $openpay, $token_id, $device_session_id);
			error_log("Agregar tarjeta a usuario en Openpay ".$id_card);
			$id_subscription	= subscribeUser($id_openpay, $data_plan["id"], $id_card, $openpay);
			error_log("Suscribir a usuario en Openpay: ".$id_subscription);
			$data_plan["id_subscription"] = $id_subscription;
		}else{
			error_log("si hay id de openpay se agrega la tarjeta y se suscribe");
			$id_card 					= addCardToUser($id_openpay_user, $openpay, $token_id, $device_session_id);
			$id_subscription	= subscribeUser($id_openpay_user, $data_plan["id"], $id_card, $openpay);
			$data_plan["id_subscription"] = $id_subscription;
		}

		deleteAllPay($id_user, $this->db);
		$sql	= "INSERT INTO pay (id_user, `date`, amount, description, id_openpay, active) VALUES (:id_user, :fecha, :amount, :description, :id_openpay, :active)";
		$sth 			= $this->db->prepare($sql);
		$active 	= 1;
		$sth->bindParam("id_user", $id_user);
		$sth->bindParam("amount", $data_plan["amount"]);
		$sth->bindParam("description", $data_plan["name"]);
		$sth->bindParam("fecha", $fecha);
		$sth->bindParam("active", $active);
		$sth->bindParam("id_openpay", $data_plan["id_subscription"]);
		$sth->execute();

		$http_response			= 200;
		$data["error"]			= 0;
		$data["description"] 	= "Éxito en la transacción, el usuario se ha suscrito con éxito";

	/*$amount_m							= 119.00;
	$amount_a							= 1190.00;
	$description_mensual 	= "Suscripción mensual en la plataforma Sereyd";
	$description_anual	 	= "Suscripción anual en la plataforma Sereyd";
	$http_response				= 500;
	$id_user							= $args["id_user"];
	$fecha 								= date("Y-m-d H:i:s");
	$data["error"]				= 1;

	try {
		$data 			= array();
		$input 			= $request->getParsedBody();

		Openpay::setId($merchant_id);
		Openpay::setApiKey($private_key);
		$openpay 		= Openpay::getInstance($merchant_id, $private_key);
		//Openpay::setProductionMode(true);

		$amount 			= $amount_m;
		$description 	= $description_mensual;

		$sql 			= "INSERT INTO pay (id_user, `date`, amount, description, id_openpay) VALUES (:id_user, :fecha, :amount, :description, :id_openpay)";
		$sth 			= $this->db->prepare($sql);

		//1 para anual
		//0 para mensual
		$type 		= intval($input["type"]);
		switch ($type) {
			case 1:
				$amount 									= $amount_a;
				$description 							= $description_anual;
				$data["amount"]						= $amount_a;
				$data["description_pay"]	= $description_anual;
				break;
			case 0:
				$amount 									= $amount_m;
				$description 							= $description_mensual;
				$data["amount"]						= $amount_m;
				$data["description_pay"]	= $description_mensual;
				break;
			default:
				break;
		}

		$customer = getDataCostumer($id_user, $this->db);

		$customerData = array(
			'name' => $customer->name,
			'last_name' => '.',
			'email' => $customer->email,
			'phone_number' => '2228629948',
			'address' => array(
				'line1' => 'Cerrada Eduardo Hay No. 6',
				'line2' => 'Col. Pino Suárez',
				'line3' => '',
				'postal_code' => '72020',
				'state' => 'Puebla',
				'city' => 'Puebla.',
				'country_code' => 'MX'));

		//$customer = $openpay->customers->add($customerData);

		$chargeData = array(
			'method' => 'card',
			'source_id' => $input["token_id"],
			'amount' => $amount,
			'description' => $description,
			'device_session_id' => $input["device_session_id"],
			//'device_session_id' => $input["deviceIdHiddenFieldName"],
			'customer' => $customerData
		);

		$charge 				= $openpay->charges->create($chargeData);

		$http_response			= 200;
		$data["error"]			= 0;
		$data["description"] 	= "Éxito en la transacción";

		$sth->bindParam("id_user", $id_user);
		$sth->bindParam("amount", $amount);
		$sth->bindParam("description", $description);
		$sth->bindParam("fecha", $fecha);
		$sth->bindParam("id_openpay", $charge->id);
		$sth->execute();*/


	}catch (OpenpayApiTransactionError $e) {
	/*error_log('ERROR on the transaction: ' . $e->getMessage() .
	      ' [error code: ' . $e->getErrorCode() .
	      ', error category: ' . $e->getCategory() .
	      ', HTTP code: '. $e->getHttpCode() .
	      ', request ID: ' . $e->getRequestId() . ']', 0);*/
	$data["description"] 	= 'ERROR on the transaction: ' . $e->getMessage() .
	      ' [error code: ' . $e->getErrorCode() .
	      ', error category: ' . $e->getCategory() .
	      ', HTTP code: '. $e->getHttpCode() .
	      ', request ID: ' . $e->getRequestId() . ']';
	} catch (OpenpayApiRequestError $e) {
		//error_log('ERROR on the request: ' . $e->getMessage(), 0);
		$data["description"] 	= 'ERROR on the request: ' . $e->getMessage();
	} catch (OpenpayApiConnectionError $e) {
		//error_log('ERROR while connecting to the API: ' . $e->getMessage(), 0);
		$data["description"] 	= 'ERROR while connecting to the API: ' . $e->getMessage();
	} catch (OpenpayApiAuthError $e) {
		//error_log('ERROR on the authentication: ' . $e->getMessage(), 0);
		$data["description"] 	= 'ERROR on the authentication: ' . $e->getMessage();
	} catch (OpenpayApiError $e) {
		//error_log('ERROR on the API: ' . $e->getMessage(), 0);
		$data["description"] 	= 'ERROR on the API: ' . $e->getMessage();
	} catch (Exception $e) {
		//error_log('Error on the script: ' . $e->getMessage(), 0);
		$data["description"] 	= 'Error on the script: ' . $e->getMessage();
	} catch (PDOException $e) {
		$data["description"] 	= 'Error on the DB: ' . $e->getMessage();
	}

	return $this->response->withJson($data, $http_response);

});


/*$app->post('/openpay/[{id_user}]', function ($request, $response, $args) {

	$merchant_id 			= 'm0nccdfwzr0pf8mdiuhd'; //Pruebas
	$private_key 			= 'sk_dcbbdbcc3d564392926d3fd92ce4b6e3'; //Pruebas

	//$merchant_id 			= 'mbljemtdh8osybgrddpt'; //Producción
	//$private_key 			= 'sk_43d8e50871cd40a085e3b38a437a7933'; //Producción

	$amount_m							= 119.00;
	$amount_a							= 1190.00;
	$description_mensual 	= "Suscripción mensual en la plataforma Sereyd";
	$description_anual	 	= "Suscripción anual en la plataforma Sereyd";
	$http_response				= 500;
	$id_user							= $args["id_user"];
	$fecha 								= date("Y-m-d H:i:s");
	$data["error"]				= 1;

	try {
		$data 			= array();
		$input 			= $request->getParsedBody();

		Openpay::setId($merchant_id);
		Openpay::setApiKey($private_key);
		$openpay 		= Openpay::getInstance($merchant_id, $private_key);
		//Openpay::setProductionMode(true);

		$amount 			= $amount_m;
		$description 	= $description_mensual;

		$sql 			= "INSERT INTO pay (id_user, `date`, amount, description, id_openpay) VALUES (:id_user, :fecha, :amount, :description, :id_openpay)";
		$sth 			= $this->db->prepare($sql);

		//1 para anual
		//0 para mensual
		$type 		= intval($input["type"]);
		switch ($type) {
			case 1:
				$amount 									= $amount_a;
				$description 							= $description_anual;
				$data["amount"]						= $amount_a;
				$data["description_pay"]	= $description_anual;
				break;
			case 0:
				$amount 									= $amount_m;
				$description 							= $description_mensual;
				$data["amount"]						= $amount_m;
				$data["description_pay"]	= $description_mensual;
				break;
			default:
				break;
		}

		$customer = getDataCostumer($id_user, $this->db);

		$customerData = array(
			'name' => $customer->name,
			'last_name' => '.',
			'email' => $customer->email,
			'phone_number' => '2228629948',
			'address' => array(
				'line1' => 'Cerrada Eduardo Hay No. 6',
				'line2' => 'Col. Pino Suárez',
				'line3' => '',
				'postal_code' => '72020',
				'state' => 'Puebla',
				'city' => 'Puebla.',
				'country_code' => 'MX'));

		//$customer = $openpay->customers->add($customerData);

		$chargeData = array(
			'method' => 'card',
			'source_id' => $input["token_id"],
			'amount' => $amount,
			'description' => $description,
			'device_session_id' => $input["device_session_id"],
			//'device_session_id' => $input["deviceIdHiddenFieldName"],
			'customer' => $customerData
		);

		$charge 				= $openpay->charges->create($chargeData);

		$http_response			= 200;
		$data["error"]			= 0;
		$data["description"] 	= "Éxito en la transacción";

		$sth->bindParam("id_user", $id_user);
		$sth->bindParam("amount", $amount);
		$sth->bindParam("description", $description);
		$sth->bindParam("fecha", $fecha);
		$sth->bindParam("id_openpay", $charge->id);
		$sth->execute();


	}catch (OpenpayApiTransactionError $e) {
	/*error_log('ERROR on the transaction: ' . $e->getMessage() .
	      ' [error code: ' . $e->getErrorCode() .
	      ', error category: ' . $e->getCategory() .
	      ', HTTP code: '. $e->getHttpCode() .
	      ', request ID: ' . $e->getRequestId() . ']', 0);
	$data["description"] 	= 'ERROR on the transaction: ' . $e->getMessage() .
	      ' [error code: ' . $e->getErrorCode() .
	      ', error category: ' . $e->getCategory() .
	      ', HTTP code: '. $e->getHttpCode() .
	      ', request ID: ' . $e->getRequestId() . ']';
	} catch (OpenpayApiRequestError $e) {
		error_log('ERROR on the request: ' . $e->getMessage(), 0);
		$data["description"] 	= 'ERROR on the request: ' . $e->getMessage();
	} catch (OpenpayApiConnectionError $e) {
		//error_log('ERROR while connecting to the API: ' . $e->getMessage(), 0);
		$data["description"] 	= 'ERROR while connecting to the API: ' . $e->getMessage();
	} catch (OpenpayApiAuthError $e) {
		//error_log('ERROR on the authentication: ' . $e->getMessage(), 0);
		$data["description"] 	= 'ERROR on the authentication: ' . $e->getMessage();
	} catch (OpenpayApiError $e) {
		//error_log('ERROR on the API: ' . $e->getMessage(), 0);
		$data["description"] 	= 'ERROR on the API: ' . $e->getMessage();
	} catch (Exception $e) {
		//error_log('Error on the script: ' . $e->getMessage(), 0);
		$data["description"] 	= 'Error on the script: ' . $e->getMessage();
	} catch (PDOException $e) {
		$data["description"] 	= 'Error on the DB: ' . $e->getMessage();
	}

	return $this->response->withJson($data, $http_response);

});*/


/*$app->post('/openpay', function ($request, $response, $args) {

	$input 			= $request->getParsedBody();
	$id_user 		= $input["id_user"];

	$merchant_id 			= 'm0nccdfwzr0pf8mdiuhd'; //Pruebas
	$private_key 			= 'sk_dcbbdbcc3d564392926d3fd92ce4b6e3'; //Pruebas

	try {
		Openpay::setId($merchant_id);
		Openpay::setApiKey($private_key);
		$openpay 		= Openpay::getInstance($merchant_id, $private_key);




	} catch (OpenpayApiRequestError $e) {
		error_log('ERROR on the request: ' . $e->getMessage(), 0);
		$g["description"] 	= 'ERROR on the request: ' . $e->getMessage();
	} catch (OpenpayApiConnectionError $e) {
		//error_log('ERROR while connecting to the API: ' . $e->getMessage(), 0);
		$g["description"] 	= 'ERROR while connecting to the API: ' . $e->getMessage();
	} catch (OpenpayApiAuthError $e) {
		//error_log('ERROR on the authentication: ' . $e->getMessage(), 0);
		$g["description"] 	= 'ERROR on the authentication: ' . $e->getMessage();
	} catch (OpenpayApiError $e) {
		//error_log('ERROR on the API: ' . $e->getMessage(), 0);
		$g["description"] 	= 'ERROR on the API: ' . $e->getMessage();
	} catch (Exception $e) {
		//error_log('Error on the script: ' . $e->getMessage(), 0);
		$g["description"] 	= 'Error on the script: ' . $e->getMessage();
	} catch (PDOException $e) {
		$g["description"] 	= 'Error on the DB: ' . $e->getMessage();
	}


	return $this->response->withJson($g, 200);

});*/


function deleteAllPay($id_user, $db){

		$sql = "UPDATE pay SET active=0 WHERE id_user=:id_user";
		try{
			$sth = $db->prepare($sql);
			$sth->bindParam("id_user", $id_user);
			$sth->execute();
			//$c = $sth->fetchObject();
			return $c;
		}catch(PDOException $e){
			return -1;
		}
}

function subscribeUser($id_user, $id_plan, $id_card, $openpay){

	$subscriptionDataRequest = array(
    'plan_id' => $id_plan,
    'card_id' => $id_card
	);

		$customer 		= $openpay->customers->get($id_user);
		$subscription = $customer->subscriptions->add($subscriptionDataRequest);
		return $subscription->id;

}


function addCardToUser($id_openpay, $openpay, $token_id, $device_session_id){

	$cardDataRequest = array(
    'token_id' 					=> $token_id,
    'device_session_id' => $device_session_id
	);

	$customer = $openpay->customers->get($id_openpay);
	$card 		= $customer->cards->add($cardDataRequest);

	return $card->id;

}

function createUserOpenpay($name, $email, $id_user, $openpay, $db) {

	$customerData = array(
     'name' => $name,
     'email' => $email
   );

	$customer 	= $openpay->customers->add($customerData);
	$id_openpay = $customer->id;
	$sql = "UPDATE user SET id_openpay=:id_openpay WHERE id_user=:id_user";
	try {
		$sth = $db->prepare($sql);
		$sth->bindParam("id_user", $id_user);
		$sth->bindParam("id_openpay", $id_openpay);
		$sth->execute();
		//$c = $sth->fetchObject();
		return $id_openpay;
	} catch (Exception $e) {
		error_log("Error creando el usuario en Openpay : ".$e->getMessage());
		return -1;
	}
}

function getDataCostumer($id_user, $db){

	$sql = "SELECT name, email, id_openpay FROM user WHERE id_user=:id_user";
	try{
		$sth = $db->prepare($sql);
		$sth->bindParam("id_user", $id_user);
		$sth->execute();
		$c = $sth->fetchObject();
		return $c;
	}catch(PDOException $e){
		return -1;
	}
}

function getDataPlan($id_plan, $db) {

	$sql = "SELECT name, amount FROM subscription WHERE id_openpay=:id_openpay";
	try {
			$sth = $db->prepare($sql);
			$sth->bindParam("id_openpay", $id_plan);
			$sth->execute();
			$c = $sth->fetchObject();
			return $c;
	} catch (PDOException $e) {
		return $e->getMessage();
	}

}

function getCountPay($id_user, $db){

	$sql = "SELECT count(*) as count FROM pay WHERE id_user=:id_user";
	$sth = $db->prepare($sql) ;

	try {
		$sth->bindParam("id_user", $id_user);
		$sth->execute();
		$count = $sth->fetchObject();
		return $count->count;
	} catch (PDOException $e) {
		error_log($e->getMessage());
		return -1;
	}


}

function getOpenPayData($id_user, $db, $openpay){

	//$sql 	= "SELECT * FROM pay where id_user=:id_user ORDER BY date DESC LIMIT 1";
	$sql 	= "SELECT p.*, u.id_openpay as id_openpay_user FROM user u INNER JOIN pay p ON u.id_user= p.id_user WHERE p.id_user=:id_user AND active=1 ORDER BY p.date DESC LIMIT 1";
	$sth 	= $db->prepare($sql);
	$r 		= array();
	try {
		$sth->bindParam("id_user", $id_user);
		$sth->execute();
		$dataOP = $sth->fetchObject();
		$r["error"]					= 0;
		$r["cancelled"]			= 0;
		$id 								= $dataOP->id_openpay;
		$id_openpay_user 		= $dataOP->id_openpay_user;
		if(strlen($id)<=0){
			//el id esta vacio
			$history_pay_count = getCountPay($id_user, $db);
			$history_pay_count = intval($history_pay_count);
			if ($history_pay_count < 0) {
				//error en la transacción
				$r["error"] = 1;
				$r["description"] = "Error obteniendo el contador de pagos";
			}elseif ($history_pay_count > 0) {
				//si existe un historial de pago
				$r["desc"] 					= "Este usuario ha cancelado suscripción";
				$r["status"]				= "cancelled";
				$r["cancelled"]			= 1;
			}elseif ($history_pay_count==0) {
				//pago con paypal
				$r["desc"] 					= "Este usuario realizó un pago mediante Paypal";
				$r["status"]				= "paypal";
			}

		}else{
			//el id no esta vacio
			//$t 									= $openpay->charges->get($id);
			/*$r["card_number"] 	= $t->card->card_number;
			$r["amount"] 				= $t->amount;
			$r["card_number"] 	= $t->card->card_number;
			$r["holder_name"] 	= $t->card->holder_name;
			$r["desc"] 					= $t->description;*/

			$customer 					= $openpay->customers->get($id_openpay_user);
			$subscription 			= $customer->subscriptions->get($id);
			$plan_id 						= $subscription->plan_id;
			$r["card_number"] 	= $subscription->card->card_number;
			$r["holder_name"] 	= $subscription->card->holder_name;
			$r["charge_date"] 	= $subscription->charge_date;
			$r["status"] 				= $subscription->status;

			$plan 							= $openpay->plans->get($plan_id);

			$r["desc"] 					= $plan->name;
			$r["amount"] 				= $plan->amount;
		}
	} catch (Exception $e) {
		$r["error"] = 1;
		$r["description"] = "Error obteniendo el contador de pagos ".$e->getMessage();
	}

	return $r;

}
