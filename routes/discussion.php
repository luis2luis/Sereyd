<?php


$app->get('/discussion', function($request, $response, $args){

  $sql            = "SELECT d.*, u.name, u.image FROM discussion d INNER JOIN user u ON d.id_user=u.id_user";
  $http_response 	= 200;
  $data           = array();

  try {
    $sth        = $this->db->prepare($sql);
    $sth->execute();
    $questions  = $sth->fetchAll();
    $data["error"]      = 0;
    $data["questions"]  = $questions;
  } catch (PDOException $e) {
    $data["error"]        = 0;
    $data["description"]  = "Un error ha ocurrido: ".$e->getMessage();
    $http_response 	= 200;
  }
  return $this->response->withJson($data, $http_response);


});

$app->post('/discussion', function ($request, $response, $args) {

  $input 					= $request->getParsedBody();
  $title          = $input["title"];
  $question       = $input["question"];
  $id_user        = $input["id_user"];
  $date           = date('Y-m-d');
  $http_response 	= 200;
  $sql 						= "INSERT INTO discussion (title, question, date_creation, id_user) VALUES (:title, :question, :date_creation, :id_user)";

  try {
    $sth = $this->db->prepare($sql);
    $sth->bindParam("title", $title);
    $sth->bindParam("question", $question);
    $sth->bindParam("date_creation", $date);
    $sth->bindParam("id_user", $id_user);
    $sth->execute();
    $input["id_discussion"] = $this->db->lastInsertId();
    $input["date_creation"] = $date;
    if ($input["id_discussion"] != false) {
      $data["error"]        = 0;
      $data["description"]  = "Entrada de foro creada con éxito";
      $data["discussion"]   = $input;
    }else{
      $data["error"]       = 1;
      $http_response 	     = 500;
      $data["description"] = "Error al crear entrada de foro";
    }
  } catch (PDOException $e) {
    $data["error"]        = 1;
    $http_response 	      = 500;
    $data["description"]  = $e->getMessage();
  }
  return $this->response->withJson($data, $http_response);

});

$app->get('/discussion/[{id_discussion}]', function($request, $response, $args){

  $id_discussion = $args["id_discussion"];

  //$sql = "SELECT title, question, date_creation FROM discussion WHERE id_discussion=:id_discussion" ;
  $sql = "SELECT d.*, u.name, u.image FROM discussion d INNER JOIN user u ON d.id_user=u.id_user WHERE d.id_discussion=:id_discussion" ;

  try {
    $sth = $this->db->prepare($sql);
    $sth->bindParam("id_discussion", $id_discussion);
    $sth->execute();
    $discussion = $sth->fetchObject();
    $data["error"]       = 0;
    $data["description"] = "Éxito al obtener la pregunta y respuestas del foro";
    $data["discussion"]  = $discussion;
    $data["answers"]     = getAnswersByDiscussion($id_discussion, $this->db);
  } catch (PDOException $e) {
    $data["error"]        = 1;
    $http_response        = 500;
    $data["description"]  = "error al obtener el diaro de la educadora";
  }
  return $this->response->withJson($data, $http_response);

});


function getAnswersByDiscussion($id_discussion, $db){

  $sql = "SELECT a.id_answer, a.answer, a.ranking, a.date_creation, u.name, u.image FROM answer a INNER JOIN user u ON a.id_user = u.id_user WHERE a.id_discussion = :id_discussion";
  try{
    $sth = $db->prepare($sql);
    $sth->bindParam("id_discussion", $id_discussion);
    $sth->execute();
    return $sth->fetchAll();
  }catch(PDOException $e){
      return [];
  }

}
