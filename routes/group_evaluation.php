<?php

require __DIR__ . '/../lib/Classes/PHPExcel.php';

$app->post('/group_evaluation', function ($request, $response, $args) {

	$data = array();
	$http_response = 200;
	$fecha = date('Y-m-d');
	$input = $request->getParsedBody();
	$sql = "INSERT INTO group_evaluation (id_evaluation, id_group, created_at) VALUES (:id_evaluation, :id_group, :created_at)";
	$sth = $this->db->prepare($sql);
	try{
		$sth->bindParam("id_evaluation", $input['id_evaluation']);
		$sth->bindParam("id_group", $input['id_group']);
		$sth->bindParam("created_at", $fecha);
		$sth->execute();
		$id = $this->db->lastInsertId();
		if($id != null){
			$data["error"] = 0;
			$data["description"] = "Grupo y evaluación asociados con éxito";
			$input["completed"] = 0;
			$input["created_at"] = $fecha;
			$input["id_group_evaluation"] = $this->db->lastInsertId();
			$data["group_evaluation"] = $input;
		}else{
			$data["error"] = 0;
			$http_response = 500;
		}
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response);
});


$app->put('/group_evaluation/completed/[{id_group_evaluation}]', function ($request, $response, $args) {

	$data = array();
	$http_response = 200;
	try{
		$sth = $this->db->prepare("UPDATE group_evaluation SET completed=1 WHERE id_group_evaluation = :id_group_evaluation");
		$sth->bindParam("id_group_evaluation", $args["id_group_evaluation"]);
		$band=$sth->execute();
		$data = array(
			'error' => 0,
			'completed' => $band
		);
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response);

});

$app->get('/group_evaluation/[{id_group_evaluation}]', function($request, $response, $args){

	$data = array();
	$http_response = 200;
	$id_group_evaluation = $args["id_group_evaluation"];

	$id_group = getGroupByIdEvaluation($id_group_evaluation, $this->db);
	//primero obtengo todos los alumnos de un grupo
	$studentsOnGroup = getAllStudentsOnGroup($id_group->id, $this->db);
	$students_in_group = [];
	foreach ($studentsOnGroup as $k) {
		$students_in_group[] = $k["id_student"];
	}

	//despues obtengo los alumnos que ya ha sido calificados en este group_evaluation
	$studentsInEvaluation = getAllStudentsOnEvaluation($id_group_evaluation, $this->db);
	$students_in_evaluation = [];
	foreach ($studentsInEvaluation as $ki) {
		$students_in_evaluation[] = $ki["id_student"];
	}
	//por ultimo hago una resta del primer array obtenido menos el segundo
	$students_pending = [];
	for ($i=0; $i < count($students_in_group); $i++){

		$b = array_search($students_in_group[$i], $students_in_evaluation);
		if($b===false){
			array_push($students_pending,$students_in_group[$i]);
		}
	}



	$data["error"] = 0;
	$data["id_group"] = $id_group;
	$data["students_in_group"] = $students_in_group;
	$data["students_completed"] = $students_in_evaluation;
	$data["students_pending"] = $students_pending;


	return $this->response->withJson($data, $http_response);

});


$app->get('/group_evaluation/final/[{id_group_evaluation}]', function($request, $response, $args){

	$data 					= array();
	$http_response 			= 200;
	$input 					= $request->getParsedBody();
	$id_group_evaluation 	= $args["id_group_evaluation"];
	$ge 					= getGroupByIdEvaluation($id_group_evaluation, $this->db);
	$id_group 				= $ge->id;
	$ans 					= getAllAnswersEvaluation($id_group_evaluation, $this->db);
	$answers 				= $ans["answers"];
	$count_yes 				= 0;


	foreach ($answers as $key) {
		if($key["answer"]==1)
			$count_yes++;
	}


	$data["error"] = 0;
	$data["id_group"] = $id_group;
	$data["created_at"] = $ge->created_at;
	$data["evaluation_percent"] = ($count_yes*100)/count($answers);
	$data["evaluation_name"] = getNameEvaluation($ge->id_evaluation, $this->db);


	return $this->response->withJson($data, $http_response);

});

$app->get('/group_evaluation/pending/[{id_group}]', function($request, $response, $args){

	$data = array();
	$http_response = 200;

	try{
		$sql = "SELECT id_group_evaluation, completed, id_evaluation, created_at FROM group_evaluation WHERE id_group=:id_group";
		$sth = $this->db->prepare($sql);
		$sth->bindParam("id_group", $args["id_group"]);
		$sth->execute();
		$d = $sth->fetchAll();
		$data = array(
			'error' => 0,
			'evaluations' => $d
		);
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response);
});

$app->get('/group_evaluation/file/[{id_group_evaluation}]', function ($request, $response, $args) {

	$data = array();
	$http_response = 200;

	try {

		$filename 									= "test";
		$id_group_evaluation 				= $args['id_group_evaluation'];
		$id_evaluation 							= getIdEvaluation($id_group_evaluation, $this->db);
		$questions_for_evaluation 	= getQuestionsForEvaluation($id_evaluation, $this->db);
		$info_group									= getInfoGroupEvaluation($id_group_evaluation, $this->db);

		//Preguntas
		$table	= "<table><tr><th>".$info_group["school"]."</th><th>".$info_group["grade"]."</th><th>".$info_group["group_number"]."</th><th>".$info_group["period"]."</th></tr>";
		$table .= "<tr><th style='color: #F00'>No.</th><th>Nombre y Apellido</th>";
	    foreach ($questions_for_evaluation as $k) {
	    	$table .= "<th colspan='3'>".$k["question"]."</th>";
	    }
	    $table .= "</tr>";
	    $table .= "<tr><td></td><td></td><td>Si</td><td>No</td><td>En Proceso</td><td>Si</td><td>No</td><td>En Proceso</td><td>Si</td><td>No</td><td>En Proceso</td><td>Si</td><td>No</td><td>En Proceso</td><td>Si</td><td>No</td><td>En Proceso</td></tr>";

		$students = getStudentsFromEvaluation($id_group_evaluation, $this->db);
		$reset 			= 0;
		$student_count  = 1;
		$student_actual = "";

	    foreach ($students as $z) {

			if(($reset % 5) == 0){
				$student_actual = $z["fullname"];
				$table .= "<tr><td>".$student_count."</td><td>".$student_actual."</td>";
				$table .= answerHTML(intval($z["answer"]));
				$student_count++;
			}else{
				$table .= answerHTML(intval($z["answer"]));
			}
			$reset++;
	    }

	    $tmpfile 				= __DIR__.'/../public/assets/xls/html/'.time().'.html';
		file_put_contents($tmpfile, $table);
		chmod($tmpfile, 777);
		if(file_exists($tmpfile)){
			$objPHPExcel 		= new PHPExcel();

			$styleArray = array(
				'font'  => array(
	        'bold'  => true,
	        'color' => array('rgb' => '000000'),
	        'size'  => 12
			));

			$objPHPExcel->getActiveSheet()
					->getStyle('A1:Q2')
					->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()
					->setARGB('FDC100');

			$objPHPExcel->getActiveSheet()->getStyle('A1:Q2')->applyFromArray($styleArray);

			$objPHPExcel->getActiveSheet()
					->getStyle('A3:Q3')
					->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()
					->setARGB('FAE4D3');

			for ($i=0; $i < (count($students)/5) ; $i++) {
		 			if( ($i%2)!=0){
		 				$n = $i+4;
		 				$objPHPExcel->getActiveSheet()
		 						->getStyle('A'.$n.':CN'.$n)
		 						->getFill()
		 						->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
		 						->getStartColor()
		 						->setARGB('FAE4D3');
		 			}
		 	}

					//style


			$excelHTMLReader 	= PHPExcel_IOFactory::createReader('HTML');
			$excelHTMLReader->loadIntoExisting($tmpfile,$objPHPExcel);
			$objWriter 			= new PHPExcel_Writer_Excel2007($objPHPExcel);
			$name_file 			= time().'.xlsx';
			$path 				= '/../public/assets/xls/'.$name_file;
			$objWriter->save(__DIR__.$path);

			$data["error"] 			= 0;
			$data["path"] 			= "/assets/xls/".$name_file;
			$data["path_"] 			= $tmpfile;
			$data["file_exists"] 	= file_exists($tmpfile);

			unlink($tmpfile);


		}else{
			$data["error"] 			= 1;
			$data["path"] 			= $tmpfile;
			$http_response 			= 500;
			$data["file_exists"] 	= file_exists($tmpfile) ;
		}

	} catch (Exception $e) {
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}

	return $this->response->withJson($data, $http_response);

});

function getAllAnswersEvaluation($id_group_evaluation, $db){

	$data = array();
	try{
		$sql = "SELECT * FROM question_evaluation_student WHERE id_group_evaluation=:id_group_evaluation";
		$sth = $db->prepare($sql);
		$sth->bindParam("id_group_evaluation", $id_group_evaluation);
		$sth->execute();
		$c = $sth->fetchAll();
		$data = array(
			'error' => 0,
			'answers' => $c
		);
	}catch(PDOException $e){
		$data["error"] = 1;
		$data["description"] = $e->getMessage();
	}
	return $data;

}

function getNameEvaluation($id_evaluation, $db){

	$sql = "SELECT name FROM evaluation WHERE id_evaluation=:id_evaluation";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_evaluation", $id_evaluation);
		$sth->execute();
		$obj = $sth->fetchObject();
		return $obj->name;
	}catch(PDOException $e){
		return false;
	}
}


function getGroupByIdEvaluation($id_group_evaluation, $db){

	$sql = "SELECT id_group as id, created_at, id_evaluation FROM group_evaluation WHERE id_group_evaluation=:id_group_evaluation";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_group_evaluation", $id_group_evaluation);
		$sth->execute();
		$obj = $sth->fetchObject();
		return $obj;
	}catch(PDOException $e){
		return false;
	}
}


function  getAllStudentsOnEvaluation($id_group_evaluation, $db){

	$sql = "SELECT id_student FROM question_evaluation_student WHERE id_group_evaluation=:id_group_evaluation group by id_student";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_group_evaluation", $id_group_evaluation);
		$sth->execute();
		$obj = $sth->fetchAll();
		return $obj;
	}catch(PDOException $e){
		return false;
	}
}

function getInfoGroupEvaluation($id_group_evaluation, $db){

	$data = array();
	try {
		$sql = "SELECT g.* FROM `group` g INNER JOIN group_evaluation ge ON ge.id_group=g.id_group WHERE ge.id_group_evaluation=:id_group_evaluation";
		$sth = $db->prepare($sql);
		$sth->bindParam("id_group_evaluation", $id_group_evaluation);
		$sth->execute();
		$c = $sth->fetchObject();
		$data["error"] 				= 0;
		$data["group"] 				= $c;
		$data["school"] 			= $c->school;
		$data["grade"] 				= $c->grade;
		$data["group_number"] = $c->group_number;
		$data["period"] 			= $c->period;
	} catch (PDOException $e) {
		$data["error"] = 1;
		$data["description"] = $e->getMessage();
	}
		return $data;
}
