<?php


$app->post('/login', function ($request, $response) {
	
	$data = array();
	$http_response = 200;
	$input = $request->getParsedBody();
	$sth = $this->db->prepare("SELECT id_user, email, name, image FROM user WHERE email=:email AND password=:password");
	try{
		$sth->bindParam("email", $input['email']);
		$pass = sha1($input['password']);
		$sth->bindParam("password", $pass);
		$sth->execute();
		$user = $sth->fetchObject();
		if($user != false){
			$data["error"] = 0;
			$data["description"] = "Inicio de sesión exitoso";
			$data["user"] = $user;
		}else{
			$data["error"] = 1;
			$http_response = 500;
			$data["description"] = "Error al encontrar usuario";
		}
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response);

});

$app->post('/login/facebook', function ($request, $response) {
	
	$data = array();
	$http_response = 200;
	$input = $request->getParsedBody();
	$sth = $this->db->prepare("SELECT id_user, id_facebook, email, name, image FROM user WHERE id_facebook=:id_facebook");
	try{
		$sth->bindParam("id_facebook", $input['id_facebook']);
		$sth->execute();
		$user = $sth->fetchObject();
		if($user != false){
			$data["error"] = 0;
			$data["description"] = "Inicio de sesión con facebook exitoso";
			$data["user"]  = $user;
		}else{
			$data["error"] = 1;
			$http_response = 500;
			$data["description"] = "Error al encontrar usuario";
		}
	}catch(PODException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	
	return $this->response->withJson($data, $http_response);

});