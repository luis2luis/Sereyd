<?php

$app->post('/day_planning', function ($request, $response) {

	$data = array();
	$http_response = 200;
	$input = $request->getParsedBody();
	$list = $input["list"];
	$list_response = [];
	$id_group = $input["id_group"];
	foreach ($list as $key) {
		
		$t = array();
		$t["attendance"] = insertAttendanceByStudent($key["id_student"], $key["attendance"], $id_group, $this->db);
		$t["behavior"] = insertBehaviorByStudent($key["id_student"], $key["behavior"], $id_group, $this->db);
		$list_response[] = $t;

	}
	$data["error"] = 0;
	$data["description"] = "datos ingresados con éxito";
	$data["objects_created"] = $list_response;
	return $this->response->withJson($data, $http_response);

});

function insertAttendanceByStudent($id_student, $attendance, $id_group, $db){

	$r = array();
	$sql = "INSERT INTO attendance (date_attendance, value, id_group, id_student) VALUES (:date_attendance, :value, :id_group, :id_student)";
	$sth = $db->prepare($sql);
	try{
		$date_attendance = date('Y-m-d');
		$sth->bindParam("date_attendance", $date_attendance);
		$sth->bindParam("value", $attendance);
		$sth->bindParam("id_group", $id_group);
		$sth->bindParam("id_student", $id_student);
		$sth->execute();
		$c = $db->lastInsertId();
		$r["error"] = 0;
		$r["id"] = $c;
		return $r;
	}catch(PDOException $e){
		$r["error"] = 1;
		$r["description"] = $e->getMessage();
		return $r;
	}

}

function insertBehaviorByStudent($id_student, $behavior, $id_group, $db){

	$r = array();
	$sql = "INSERT INTO behavior (date_behavior, value, id_group, id_student) VALUES (:date_behavior, :value, :id_group, :id_student)";
	$sth = $db->prepare($sql);
	try{
		$date_behavior = date('Y-m-d');
		$sth->bindParam("date_behavior", $date_behavior);
		$sth->bindParam("value", $behavior);
		$sth->bindParam("id_group", $id_group);
		$sth->bindParam("id_student", $id_student);
		$sth->execute();
		$c = $db->lastInsertId();
		$r["error"] = 0;
		$r["id"] = $c;
		return $r;
	}catch(PDOException $e){
		$r["error"] = 1;
		$r["description"] = $e->getMessage();
		return $r;
	}


}