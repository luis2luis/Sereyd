<?php



$app->get('/prueba/[{id_group_evaluation}]', function ($request, $response, $args) {

	$data = array();
	$filename = "test";
	$id_group_evaluation 		= $args['id_group_evaluation'];
	$id_evaluation 				= getIdEvaluation($id_group_evaluation, $this->db);
	$questions_for_evaluation 	= getQuestionsForEvaluation($id_evaluation, $this->db);

	//Preguntas
	$table = "<table><tr><th style='color: #F00'>No.</th><th>Nombre y Apellido</th>";
    foreach ($questions_for_evaluation as $k) {
    	$table .= "<th colspan='3'>".$k["question"]."</th>";
    }
    $table .= "</tr>";
    $table .= "<tr><td></td><td></td><td>Si</td><td>No</td><td>En Proceso</td><td>Si</td><td>No</td><td>En Proceso</td><td>Si</td><td>No</td><td>En Proceso</td><td>Si</td><td>No</td><td>En Proceso</td><td>Si</td><td>No</td><td>En Proceso</td></tr>";

    $students = getStudentsFromEvaluation($id_group_evaluation, $this->db);
    $reset 			= 0;
    $student_count  = 1;
    $student_actual = "";

    foreach ($students as $z) {

		if(($reset % 5) == 0){
			$student_actual = $z["fullname"];
			$table .= "<tr><td>".$student_count."</td><td>".$student_actual."</td>";
			$table .= answerHTML(intval($z["answer"]));
			$student_count++;
		}else{
			$table .= answerHTML(intval($z["answer"]));
		}
		$reset++;
    }

    $tmpfile 			= time().'.html';
	file_put_contents($tmpfile, $table);
	$objPHPExcel 		= new PHPExcel();
	$excelHTMLReader 	= PHPExcel_IOFactory::createReader('HTML');
	$excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
	unlink($tmpfile);
	$objWriter 			= new PHPExcel_Writer_Excel2007($objPHPExcel);
	$name_file 			= time().'.xlsx';
	$path 				= '/../public/assets/xls/'.$name_file;
	$objWriter->save(__DIR__.$path);

	$data["error"] 	= 0;
	$data["table"] 	= $students;
	$data["path"] 	= "/public/assets/xls/".$name_file;

	return $this->response->withJson($data, 200);

});

function answerHTML($answer){

	$yes 			= "<td>X</td> <td></td> <td></td>";
    $no 			= "<td></td> <td>X</td> <td></td>";
    $in_process 	= "<td></td> <td></td> <td>X</td>";
	switch ($answer) {
		case 0:
			# no
			return $no;
			break;
		case 1:
			# si
		return $yes;
			break;
		case 2:
			return $in_process;
			# en proceso.
			break;
		default:
			# code...
			break;
	}

}

function getStudentsFromEvaluation($id_group_evaluation, $db){

	$sql = "SELECT qes.id_student, concat(s.name,' ',s.lastname) as fullname FROM question_evaluation_student qes INNER JOIN student s ON qes.id_student=s.id_student WHERE id_group_evaluation=:id_group_evaluation GROUP BY s.id_student";
	$sql = "SELECT qes.*, concat(s.name,' ',s.lastname) as fullname FROM question_evaluation_student qes INNER JOIN student s ON qes.id_student=s.id_student WHERE id_group_evaluation=:id_group_evaluation";
	try {
		$sth = $db->prepare($sql);
		$sth->bindParam("id_group_evaluation", $id_group_evaluation);
		$sth->execute();
		$c = $sth->fetchAll();
		return $c;
	} catch (PDOException $e) {
		return -1;
	}
}

function getIdEvaluation($id_group_evaluation, $db){

	$sql = "SELECT id_evaluation FROM group_evaluation WHERE id_group_evaluation=:id_group_evaluation";
	try {
		$sth = $db->prepare($sql);
		$sth->bindParam("id_group_evaluation", $id_group_evaluation);
		$sth->execute();
		$c = $sth->fetchObject();
		return intval($c->id_evaluation);
	} catch (PDOException $e) {
		return -1;
	}
}

function getQuestionsForEvaluation($id_evaluation, $db){

	$sql = "SELECT id_question_evaluation, question FROM question_evaluation WHERE id_evaluation=:id_evaluation";
	try {
		$sth = $db->prepare($sql);
		$sth->bindParam("id_evaluation", $id_evaluation);
		$sth->execute();
		$c = $sth->fetchAll();
		return $c;
	} catch (PDOException $e) {
		return -1;
	}
}
