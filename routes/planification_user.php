<?php

$app->post('/planification_user/', function ($request, $response) {

	$data 			= array();
	$http_response 	= 200;
	$input 			= $request->getParsedBody();
	$id_user 		= $input['id_user'];

	$sql = "INSERT INTO `planification_user` (id_user, id_planification) VALUES (:id_user, :id_planification)";
	$sth = $this->db->prepare($sql);
	try{
		$sth->bindParam("id_user", $id_user);
		$sth->bindParam("id_planification", $input['id_planification']);
		$pay = checkPayment($id_user, $this->db);
		if($pay["error"] == 0){
			$sth->execute();
			$data["error"] = 0;
			$data["description"] = "Planeación asignada con éxito";
			updatePaymentA($id_user, $this->db);
			$data["planification_count"] = getCountPaymentA($id_user, $this->db);
		}else{
			$data["error"] = 1;
			$http_response = 500;
			if($pay["planification_count"] == 0)
				$data["description"] = "Ya no cuentas con planeaciones disponibles";
			else
				$data["description"] = "Error al asignar planeación";
			$data["planification_count"] = $pay["planification_count"];
		}
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}

	return $this->response->withJson($data, $http_response);

});


function updatePaymentA($id_user, $db){

	$sql = "UPDATE payment SET planification_count=planification_count-1 WHERE id_user = :id_user";
	$r = array();
	$sth = $db->prepare($sql);
	try{		
		$sth->bindParam("id_user", $id_user);
		$band = $sth->execute();
		if($band!=false){
			$r["error"] = 0;
		}else{
			$r["error"] = 1;
		}
		return $r;

	}catch(PDOException $e){

		$r["error"] = 1;
		$r["description"] = "Error al modificar el pago ". $e->getMessage();
		return $r;
	}

}


function checkPayment($id_user, $db){

	$sql = "SELECT planification_count, resource_count FROM payment WHERE id_user=:id_user";
	$r = array();
	$sth = $db->prepare($sql);
	try{

		$sth->bindParam("id_user", $id_user);
		$sth->execute();
		$payments = $sth->fetchObject();
		if($payments == null){
			$r["error"] = 1;
			$r["payments"] = 0;
			$r["planification_count"] 	= $payments->planification_count;
		}else{
			$r["error"] = 0;
			$r["planification_count"] 	= $payments->planification_count;
			$r["resource_count"] 		= $payments->resource_count;
		}
		return $r;

	}catch(PDOException $e){

		$r["error"] = 1;
		$r["description"] = "Error obteniendo los pagos ".$e->getMessage();
		return $r;
	}
	
}

function getCountPaymentA($id_user, $db){

	$sql = "SELECT planification_count FROM payment WHERE id_user=:id_user";
	$r = array();
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_user", $id_user);
		$sth->execute();
		$payments = $sth->fetchObject();
		if($payments->planification_count == null){
			$r["error"] = 1;
			$r["payments"] = -1;
		}else{
			$r["error"] = 0;
			$r["payments"] = $payments->planification_count;
		}
		return $r;

	}catch(PDOException $e){

		$r["error"] = 1;
		$r["description"] = "Error obteniendo el contador de pagos ".$e->getMessage();
		return $r;
	}
	
}