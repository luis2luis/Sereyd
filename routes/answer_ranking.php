<?php

date_default_timezone_set('America/Mexico_City');

$app->post('/answer_ranking', function($request, $response, $args){

  $input 					= $request->getParsedBody();
  $id_user        = $input["id_user"];
  $id_answer      = $input["id_answer"];
  $ranking        = $input["ranking"];
  $date           = date("Y-m-d H:i:s");
  $http_response 	= 200;
  $sql 						= "INSERT INTO answer_ranking (id_user, id_answer, ranking, date_creation) VALUES (:id_user, :id_answer, :ranking, :date_creation)";

  $count = checkAnswerRanking($id_answer, $id_user, $this->db);
  if(intval($count) > 0){
    $http_response 	      = 401;
    $data["error"]        = 1;
    $data["description"]  = "Este usuario ya ha calificado esta respuesta :(";
  }else{
    try {
      $sth = $this->db->prepare($sql);
      $sth->bindParam("id_answer", $id_answer);
      $sth->bindParam("ranking", $ranking);
      $sth->bindParam("date_creation", $date);
      $sth->bindParam("id_user", $id_user);
      $sth->execute();
      $input["id_answer_ranking"] = $this->db->lastInsertId();
      $input["date_creation"] = $date;
      if ($input["id_answer"] != false) {
        $data["error"]            = 0;
        $data["description"]      = "Ranking creado con éxito";
        $data["answer_ranking"]   = $input;
        $t = updateRankingByAnswer($id_answer, $this->db);
      }else{
        $data["error"]       = 1;
        $http_response 	     = 500;
        $data["description"] = "Error al crear ranking";
      }
    } catch (PDOException $e) {
      $data["error"]        = 1;
      $http_response 	      = 500;
      $data["description"]  = $e->getMessage();
    }
  }
  return $this->response->withJson($data, $http_response);


});


function checkAnswerRanking($id_answer, $id_user, $db){


  $sql = "SELECT count(*) as count FROM answer_ranking WHERE id_answer=:id_answer AND id_user=:id_user";

  try {
    $sth = $db->prepare($sql);
    $sth->bindParam("id_answer", $id_answer);
    $sth->bindParam("id_user", $id_user);
    $sth->execute();
    $c = $sth->fetchObject();
    return $c->count;
  } catch (PDOException $e) {
    return -1;
  }

}

function updateRankingByAnswer($id_answer, $db){

  $ranking  = getRankingByAnswer($id_answer, $db);
  $sql      = "UPDATE answer SET ranking=:ranking WHERE id_answer=:id_answer";

  try {
    $sth = $db->prepare($sql);
    $sth->bindParam("id_answer", $id_answer);
    $sth->bindParam("ranking", $ranking);
    $sth->execute();
    $c = $sth->fetchObject();
    return 1;
  } catch (PDOException $e) {
    return -1;
  }

}

function getRankingByAnswer($id_answer, $db) {

  $sql = "SELECT avg(ranking) as ranking FROM answer_ranking WHERE id_answer=:id_answer";

  try {
    $sth = $db->prepare($sql);
    $sth->bindParam("id_answer", $id_answer);
    $sth->execute();
    $c = $sth->fetchObject();
    return $c->ranking;
  } catch (PDOException $e) {
    return -1;
  }
}
