<?php


$app->get('/cooperation', function ($request, $response, $args) {
	
	$sth = $this->db->prepare("SELECT id_cooperation, concept, description, amount FROM cooperation");
	$sth->execute();
	$users = $sth->fetchAll();
	return $this->response->withJson($users); 
});

$app->post('/cooperation/', function($request, $response, $args) {

	$data = array();
	$http_response = 200;
	$input = $request->getParsedBody();
	$sql = "INSERT INTO cooperation (concept, description, amount, id_group) VALUES (:concept, :description, :amount, :id_group)";
	$sth = $this->db->prepare($sql);
	try{
		$sth->bindParam("concept", $input['concept']);
		$sth->bindParam("description", $input['description']);
		$sth->bindParam("amount", $input['amount']);
		$sth->bindParam("id_group", $input['id_group']);
		$sth->execute();
		$input['id_cooperation'] = $this->db->lastInsertId();
		if($input['id_cooperation'] != false){
			$data["error"] = 0;
			$data["description"] = "Cooperación creada con éxito";
			$data["cooperation"] = $input;
		}else{
			$data["error"] = 1;
			$http_response = 500;
			$data["description"] = "Error al crear una cooperación nueva";
		}
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response);
});

$app->get('/cooperation/[{id_cooperation}]', function($request, $response, $args) {

	$data = array();
	$http_response = 200;
	$sth = $this->db->prepare("SELECT id_cooperation, concept, description, amount FROM cooperation WHERE id_cooperation=:id_cooperation");
	$sth->bindParam("id_cooperation", $args['id_cooperation']);
	$sth->execute();
	$cooperation = $sth->fetchObject();
	if(!$cooperation){
		$data["error"] = 1;
		$data["description"] = "No existe una cooperación con el identificador: ".$args['id_cooperation'];
		$http_response = 500;
	}else{
		$data["error"] = 0;
		$data["description"] = "Cooperación encontrada con éxito";
		$data["cooperation"] = $cooperation;
	}
	return $this->response->withJson($data, $http_response); 

});

$app->put('/cooperation/[{id_cooperation}]', function($request, $response, $args){
	
	$data = array();
	$http_response = 200;
	$input = $request->getParsedBody();
	$id_cooperation = $args['id_cooperation'];

	$sql = "UPDATE `cooperation` SET  concept=:concept, description=:description, amount=:amount WHERE id_cooperation=:id_cooperation";
	$sth = $this->db->prepare($sql);
	try{
		$sth->bindParam("concept", $input['concept']);
		$sth->bindParam("description", $input['description']);
		$sth->bindParam("amount", $input['amount']);
		$sth->bindParam("id_cooperation", $id_cooperation);
		$exists = cooperationExists($id_cooperation, $this->db);
		if($exists["error"]==0){
			if($exists["cooperation_contador"]==0){//no existe la tarea
				$data["error"] = 1;
				$http_response = 500;
				$data["description"] = "No se encontró una cooperación con este identificador: ".$id_cooperation;
			}else{
				$cooperation = $sth->execute();
				$data["error"] = 0;
				$data["description"] = "Cooperación modificada con éxito";
				$data["cooperation"] = $cooperation;
			}
		}else{
			$data["error"] = 1;
			$data["description"] = $exists["description"];
			$http_response = 500;
		}
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response);
});


$app->delete('/cooperation/[{id_cooperation}]', function($request, $response, $args){

	$data = array();
	$http_response = 200;
	$sth = $this->db->prepare("DELETE FROM `cooperation` WHERE id_cooperation=:id_cooperation");
	try{
		$sth->bindParam("id_cooperation", $args['id_cooperation']);
		$exists = cooperationExists($args['id_cooperation'], $this->db);
		if($exists["error"]==0){
			if($exists["cooperation_contador"]==0){//no existe la cooperación
				$data["error"] = 1;
				$http_response = 500;
				$data["description"] = "No se encontró una cooperación con este identificador: ".$args['id_cooperation'];
			}else{
				$cooperation = $sth->execute();
				$data["error"] = 0;
				$data["description"] = "Se ha eliminado una cooperación con éxito";
				$data["cooperation"] = $cooperation;
			}
		}else{
			$data["error"] = 1;
			$data["description"] = $exists["description"];
			$http_response = 500;
		}

	}catch(PDOException $e){
		
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response); 
});

function cooperationExists($id_cooperation, $db){

	$r = array();
	$sql = "SELECT count(*) as contador FROM `cooperation` WHERE id_cooperation=:id_cooperation";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_cooperation", $id_cooperation);
		$sth->execute();
		$c = $sth->fetchObject();
		$r["error"] = 0;
		$r["cooperation_contador"] = $c->contador;
		return $r;
	}catch(PDOException $e){
		$r["error"] = 1;
		$r["description"] = $e->getMessage();
		return $r;
	}
}