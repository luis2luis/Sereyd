<?php 

$app->get('/diagnostic', function ($request, $response, $args) {
	
	$data 	= array();
	$utf 	= [];
	$active = 1;
	$http_response = 200;
	$sth = $this->db->prepare("SELECT * FROM diagnostic WHERE active=:active");
	try{
		$sth->bindParam("active", $active);
		$sth->execute();
		$d = $sth->fetchAll();
		foreach ($d as $k) {
			$k['name'] = utf8_encode($k['name']);
			$utf[] = $k;
		}
		$data = array(
			'error' => 0,
			'diagnostics' => $utf
		);
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response); 

});