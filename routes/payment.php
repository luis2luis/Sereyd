<?php

$app->get('/payment', function ($request, $response, $args) {

	$sth = $this->db->prepare("SELECT * FROM payment");
	$sth->execute();
	$payments = $sth->fetchAll();
	return $this->response->withJson($payments);
});

$app->get('/payment/[{id_user}]', function ($request, $response, $args) {


	try {
		Openpay::setId(MERCHANT_ID);
		Openpay::setApiKey(PRIVATE_KEY);
		$openpay 		= Openpay::getInstance(MERCHANT_ID, PRIVATE_KEY);
		//Openpay::setProductionMode(true); //Producción

		$data 			= array();
		$http_response 	= 200;
		$id_user 		= $args['id_user'];
		$input 			= $request->getParsedBody();
		$a 					= checkTrial($id_user, $this->db);
		//code error
		//planification_count
		//resources_count

		if($a["error"] == 0){
			//el usuario tiene un periodo de prueba.
			$data["error"] 								= 0;
			$data["code"] 								= 1;
			$data["description"] 					= $a["description"];
			$data["planification_count"] 	= $a["planification_count"];
			$data["resource_count"] 			= $a["resource_count"];
			$data["trial"] 								= $a["trial"];
			$data["init_date"] 						= $a["init_date"];
			$data["finish_date"] 					= $a["finish_date"];
		}else{
			$b = checkPaymentOut($id_user, $this->db, $openpay);
			if($b["error"]==0){
				//el usuario tiene una suscripción activa
				$data["error"] 								= 0;
				$data["code"] 								= 2;
				$data["annual"] 							= $b["annual"];
				$data["init_date"] 						= $b["init_date"];
				$data["finish_date"] 					= $b["finish_date"];
				$data["description"] 					= $b["description"];
				$data["planification_count"] 	= $b["planification_count"];
				$data["resource_count"] 			= $b["resource_count"];
				//devolver tambien los datos del último pago realizado
				$data["info_pay"] 						=  getOpenPayData($id_user, $this->db, $openpay);
			}else{
				//el usuario no tiene una suscripción activa
				$data["error"] 								= 0;
				$data["code"] 								= 3;
				$data["description"] 					= "El usuario necesita realizar un pago nuevo";
				$data["planification_count"] 	= 0;
				$data["resource_count"] 			= 0;
				$data["annual"] 							= false;
			}
		}
	} catch (Exception $e) {
		$http_response				= 500;
		$data["error"] 				= 1;
		$data["description"] 	= "Hemos encontrado un error: ". $e->getMessage();
	}

	return $this->response->withJson($data, $http_response);

});

$app->post('/payment/', function($request, $response, $args) {

	$data 					= array();
	$http_response 	= 200;
	$input 					= $request->getParsedBody();
	$id_user 				= $input["id_user"];
	$type 					= $input["type"]; //1 es anual, 0 es mensual

	try{
		$count = getCountPayment($id_user, $this->db);

		if($count["payments"] < 0){
			//Nunca se ha realizado un pago con este id_user se procede a crearlo

			$paymentCreated = createPayment($id_user, $this->db, $type);
			if($paymentCreated["error"] == 0){
				$data["error"] 				= 0;
				$data["description"] 	= $paymentCreated["description"];
				$data["payment"]	 		= $paymentCreated["payment"];
			}else{
				$data["error"] 				= 1;
				$http_response 				= 500;
				$data["description"] 	= $paymentCreated["description"];
			}
		}else{
			//Ya existe un pago con este id_user asi que se modifica

			$paymentUpdated = updatePayment($id_user, $this->db, $type);
			if($paymentUpdated["error"]==0){
				$data["error"] = 0;
				$data["description"] = "Pago modificado con éxito";
			}else{
				$data["error"] = 1;
				$http_response = 500;
				$data["description"] = "Error al modificar pago";
			}
		}

	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response);
});


function createPayment($id_user, $db, $type) {

	$planification_count 	= 5;
	$resource_count 			= 20;
	$month 								= 1;

	if($type==1){
		$month 					= 12;
		$planification_count 	= 6;
		$resource_count 		= 30;
	}


	$sql = "INSERT INTO payment (`date`, planification_count, resource_count, id_user, month, annual, trial) VALUES (:fecha, :planification_count, :resource_count, :id_user, :month, :annual, :trial)";
	$r = array();
	$input = array();
	$sth = $db->prepare($sql);
	try{

		$sth->bindParam("planification_count", $planification_count);
		$sth->bindParam("resource_count", $resource_count);
		$sth->bindParam("fecha",  date('Y-m-d'));
		$sth->bindParam("id_user", $id_user);
		$sth->bindParam("month", $month);
		$sth->bindParam("annual", $type);
		$sth->execute();
		$input['id_payment'] 			= $db->lastInsertId();
		$input['date'] 					= date('Y-m-d');
		$input['planification_count'] 	= $planification_count;
		$input['resource_count'] 		= $resource_count;

		if($input['id_payment'] != false){
			$r["error"] 		= 0;
			$r["description"] 	= "Pago creado con éxito";
			$r["payment"] 		= $input;
		}else{
			$r["error"] 		= 1;
			$r["description"] 	= "Error al crear un pago nuevo";
		}
		return $r;

	}catch(PDOException $e){

		$r["error"] = 1;
		$r["description"] = "Error al crear un pago nuevo: ". $e->getMessage();
		return $r;
	}
}

function updatePayment($id_user, $db, $type){

	$planification_count 	= 5;
	$resource_count 		= 20;
	$month 					= 1;

	if($type==1){
		$month 					= 12;
		$planification_count 	= 6;
		$resource_count 		= 30;
	}

	$sql = "UPDATE payment SET `date`=:fecha, planification_count=:planification_count, resource_count=:resource_count, month=:month, trial=1 WHERE id_user = :id_user";
	$r = array();
	$fecha = date('Y-m-d');
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_user", $id_user);
		$sth->bindParam("fecha", $fecha);
		$sth->bindParam("planification_count", $planification_count);
		$sth->bindParam("resource_count", $resource_count);
		$sth->bindParam("month", $month);
		$band = $sth->execute();
		if($band!=false){
			$r["error"] = 0;
		}else{
			$r["error"] = 1;
		}
		return $r;

	}catch(PDOException $e){

		$r["error"] = 1;
		$r["description"] = "Error al modificar el pago ". $e->getMessage();
		return $r;
	}
}

function getCountPayment($id_user, $db)
{

	$sql = "SELECT planification_count FROM payment WHERE id_user=:id_user";
	$r = array();
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_user", $id_user);
		$sth->execute();
		$payments = $sth->fetchObject();
		if($payments->planification_count == null){
			$r["error"] = 1;
			$r["payments"] = -1;
		}else{
			$r["error"] = 0;
			$r["payments"] = $payments->planification_count;
		}
		return $r;

	}catch(PDOException $e){

		$r["error"] = 1;
		$r["description"] = "Error obteniendo el contador de pagos";
		return $r;
	}
}

function checkTrial($id_user, $db){

	$sql 		= "SELECT `date`, trial, planification_count, resource_count FROM payment WHERE id_user=:id_user";
	$today 		= date("Y-m-d");
	$array_info = array();

	try {
		$sth 				= $db->prepare($sql);
		$sth->bindParam("id_user", $id_user);
		$sth->execute();
		$c 					= $sth->fetchObject();
		$date_user 	= $c->date;
		$trial 			= intval($c->trial);
		$init_date 	= $c->date;

		if($trial===1){
			$array_info["error"] 		= 1;
			$array_info["description"] 	= "El periodo de prueba ha terminado";
		}else{
			$date_user 	= date_create($date_user);
			date_add($date_user, date_interval_create_from_date_string('15 days'));
			$date_user 		= date_format($date_user, 'Y-m-d');
			$finish_date 	= $date_user;
			if((strtotime($date_user) > strtotime($today)) && ($trial != 1)) {
				$array_info["error"] 								= 0;
				$array_info["description"] 					= "Este usuario se encuentra en periodo de prueba";
				$array_info["init_date"] 						= $init_date;
				$array_info["finish_date"] 					= $finish_date;
				$array_info["planification_count"] 	= intval($c->planification_count);
				$array_info["resource_count"] 			= intval($c->resource_count);
				$array_info["trial"] 								= $trial;
			}else{
				$array_info["error"] 						= 1;
				$array_info["description"] 			= "El periodo de prueba ha terminado";
				resetCountTrial($id_user, $db);
			}
		}
	} catch (PDOException $e) {
		//return -1;
		$array_info["error"] 				= 1;
		$array_info["description"] 	= "Error: ". $e->getMessage();
	}

	return $array_info;
}


function checkPaymentOut($id_user, $db, $openpay){

	$sql = "SELECT `date`, month, planification_count, resource_count FROM payment WHERE id_user=:id_user";
	$today = date("Y-m-d");
	$array_info = array();

	try{

		$sth 			= $db->prepare($sql);
		$sth->bindParam("id_user", $id_user);
		$sth->execute();
		$c 						= $sth->fetchObject();
		$date_user 		= $c->date;
		$init_date		= $c->date;
		$month 				= intval($c->month);
		$date_user 		= date_create($date_user);
		date_add($date_user, date_interval_create_from_date_string('30 days'));
		$date_user 		= date_format($date_user, 'Y-m-d');
		$finish_date 	= $date_user;

		//Obtener datos desde Openpay
		$data_openpay = getOpenPayData($id_user, $db, $openpay);
		$status 			= 0;
		switch ($data_openpay["status"]) {
			case "active":
				$status = 1;
				break;
			case "paypal":
				$status = 2;
				break;
			default:
				$status = 0;
				break;
		}


		if($month > 1){
			//anual
			if(strtotime($date_user) > strtotime($today)){
				$array_info["error"] 								= 0;
				$array_info["description"] 					= "Suscripción válida (anual)";
				$array_info["annual"] 							= true;
				$array_info["init_date"] 						= $init_date;
				$array_info["finish_date"] 					= $finish_date;
				$array_info["planification_count"] 	= intval($c->planification_count);
				$array_info["resource_count"] 			= intval($c->resource_count);
			}else{
				$array_info["error"] 								= 0;
				$array_info["description"] 					= "Suscripción válida (anual)";
				$array_info["updated"] 							= true;
				$array_info["init_date"] 						= $init_date;
				$array_info["finish_date"] 					= $finish_date;
				$array_info["annual"] 							= true;
				$array_info["planification_count"] 	= intval($c->planification_count);
				$array_info["resource_count"] 			= intval($c->resource_count);
				if($status==1){
					//tiene una Suscripción con Openpay activa.
					$resetCountsAnual 								= resetCountsAnual($id_user, $db);
				}elseif($status==2){
					//Realizó un pago anual mediante Paypal.
					$resetCountsAnual 								= resetCountsAnual($id_user, $db);
				}else{
					$resetCounts 											= resetCounts($id_user, $db);
					$deleteAllPay											= deleteAllPay($id_user, $db);
				}
			}
		}else{
			//mensual o último mes
			if((strtotime($date_user) > strtotime($today)) && ($month!=0)){
				$array_info["error"] 								= 0;
				$array_info["description"] 					= "Suscripción válida (mensual)";
				$array_info["annual"] 							= false;
				$array_info["init_date"] 						= $init_date;
				$array_info["finish_date"] 					= $finish_date;
				$array_info["planification_count"] 	= intval($c->planification_count);
				$array_info["resource_count"] 			= intval($c->resource_count);
			}else{
				if($status==1){
					//tiene una Suscripción con Openpay activa.
					$array_info["error"] 				= 0;
					$array_info["description"] 	= "Suscripción válida (mensual). Se ha renovado mediante Openpay";
					$resetCountsMensual 				= resetCountsMensual($id_user, $db);
				}elseif($status==2){
					//Realizó un pago mensual mediante Paypal.
					$array_info["error"] 				= 1;
					$array_info["description"] 	= "Se ha terminado tu suscripción";
					$resetCounts 								= resetCounts($id_user, $db);
					$deleteAllPay								= deleteAllPay($id_user, $db);
				}else{
					$array_info["error"] 				= 1;
					$array_info["description"] 	= "Se ha terminado tu suscripción";
					$resetCounts 								= resetCounts($id_user, $db);
					$deleteAllPay								= deleteAllPay($id_user, $db);
				}
			}
		}
	}catch(PDOException $e){
		$array_info["error"] 				= 1;
		$array_info["description"] 			= "Error: ". $e->getMessage();
	}
	return $array_info;
}


function resetCountsMensual($id_user, $db){

	$fecha 	= date('Y-m-d');
	$sql 	= "UPDATE payment SET `date`=:fecha, planification_count=5, resource_count=20, month=1, annual=0 WHERE id_user=:id_user";
	try{
		$sth = $db->prepare($sql);
		$sth->bindParam("id_user", $id_user);
		$sth->bindParam("fecha", $fecha);
		$c = $sth->execute();
		return 1;
	}catch(PDOException $e){
		return -1;
	}
}

function resetCountsAnual($id_user, $db){

	$fecha 	= date('Y-m-d');
	$sql 	= "UPDATE payment SET `date`=:fecha, planification_count=6, resource_count=30, month=month-1, annual=1 WHERE id_user=:id_user";
	try{
		$sth = $db->prepare($sql);
		$sth->bindParam("id_user", $id_user);
		$sth->bindParam("fecha", $fecha);
		$c = $sth->execute();
		return 1;
	}catch(PDOException $e){
		return -1;
	}
}



function resetCountTrial($id_user, $db){
	$sql = "UPDATE payment SET planification_count=0, resource_count=0, month=0, trial=1, annual=0 WHERE id_user=:id_user";
	try{
		$sth = $db->prepare($sql);
		$sth->bindParam("id_user", $id_user);
		$c = $sth->execute();
		return 1;
	}catch(PDOException $e){
		return -1;
	}
}



function resetCounts($id_user, $db){

	$sql = "UPDATE payment SET planification_count=0, resource_count=0, month=0, annual=0 WHERE id_user=:id_user";
	try{
		$sth = $db->prepare($sql);
		$sth->bindParam("id_user", $id_user);
		$c = $sth->execute();
		return 1;
	}catch(PDOException $e){
		return -1;
	}
}
