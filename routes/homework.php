<?php

$app->post('/homework/', function ($request, $response) {

	$data = array();
	$http_response = 200;
	$input = $request->getParsedBody();
	$sql = "INSERT INTO `homework` (name, description, deadline, id_group) VALUES (:name, :description, :deadline, :id_group) ";
	$sth = $this->db->prepare($sql);
	try{
		$sth->bindParam("name", $input['name']);
		$sth->bindParam("description", $input['description']);
		$sth->bindParam("deadline", $input['deadline']);
		$sth->bindParam("id_group", $input['id_group'], PDO::PARAM_INT);
		$sth->execute();

		$input['id_homework'] = $this->db->lastInsertId();
		if($input['id_homework'] != false){
			$data["error"] = 0;
			$data["description"] = "Tarea creada con éxito";
			$data["homework"] = $input;
		}else{
			$data["error"] = 1;
			$data["description"] = "Error al crear una tarea nueva";
		}
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response);
});



$app->get('/homework', function($request, $response, $args) {

	$data = array();
	$http_response = 200;
	$sth = $this->db->prepare("SELECT * FROM `homework`");
	try{
		$sth->execute();
		$homeworks = $sth->fetchAll();
		$data["error"] = 0;
		$data["description"] = "Listado de tareas obtenida con éxito";
		$data["homeworks"] = $homeworks;
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response); 
});



$app->get('/homework/[{id_homework}]', function($request, $response, $args) {

	$data = array();
	$http_response = 200;
	$sth = $this->db->prepare("SELECT * FROM `homework` WHERE id_homework=:id_homework");
	try{
		$sth->bindParam("id_homework", $args['id_homework']);
		$sth->execute();
		$homework = $sth->fetchObject();
		$data["error"] = 0;
		if($homework!=false)
			$data["description"] = "Tarea obtenida con éxito";
		else
			$data["description"] = "Tarea no encontrada";
		$data["homework"] = $homework;
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response); 
});

$app->put('/homework/[{id_homework}]', function($request, $response, $args){

	$data = array();
	$input = $request->getParsedBody();
	$http_response = 200;
	$id_homework = $args['id_homework'];
	$sql = "UPDATE `homework` SET  name=:name, description=:description, deadline=:deadline WHERE id_homework=:id_homework";
	$sth = $this->db->prepare($sql);
	try{

		$sth->bindParam("name", $input['name']);
		$sth->bindParam("description", $input['description']);
		$sth->bindParam("deadline", $input['deadline']);
		$sth->bindParam("id_homework", $args['id_homework']);
		$exists = homeworkExists($id_homework, $this->db);
		if($exists["error"]==0){
			if($exists["homework_contador"]==0){//no existe la tarea
				$data["error"] = 1;
				$http_response = 500;
				$data["description"] = "No se encontró una tarea con este identificador: ".$args['id_homework'];
			}else{
				$homework = $sth->execute();
				$data["error"] = 0;
				$data["description"] = "Tarea modificada con éxito";
				$data["homework"] = $homework;
			}
		}else{
			$data["error"] = 1;
			$data["description"] = $exists["description"];
			$http_response = 500;
		}

	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response); 
});


$app->delete('/homework/[{id_homework}]', function($request, $response, $args) {

	$data = array();
	$http_response = 200;
	$sth = $this->db->prepare("DELETE FROM `homework` WHERE id_homework=:id_homework");
	try{
		$sth->bindParam("id_homework", $args['id_homework']);
		$exists = homeworkExists($args['id_homework'], $this->db);
		if($exists["error"]==0){
			if($exists["homework_contador"]==0){//no existe la tarea
				$data["error"] = 1;
				$http_response = 500;
				$data["description"] = "No se encontró una tarea con este identificador: ".$args['id_homework'];
			}else{
				$homework = $sth->execute();
				$data["error"] = 0;
				$data["description"] = "Se ha eliminado una tarea con éxito";
				$data["homework"] = $homework;
			}
		}else{
			$data["error"] = 1;
			$data["description"] = $exists["description"];
			$http_response = 500;
		}

	}catch(PDOException $e){
		
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response); 
});


function homeworkExists($id_homework, $db){

	$r = array();
	$sql = "SELECT count(*) as contador FROM `homework` WHERE id_homework=:id_homework";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_homework", $id_homework);
		$sth->execute();
		$c = $sth->fetchObject();
		$r["error"] = 0;
		$r["homework_contador"] = $c->contador;
		return $r;
	}catch(PDOException $e){
		$r["error"] = 1;
		$r["description"] = $e->getMessage();
		return $r;
	}


}