<?php 

$app->put('/password', function ($request, $response, $args) {

	$data 			= array();
	$input 			= $request->getParsedBody();
	$http_response 	= 200;
	$email 			= $input['email'];
	$password 		= randomPassword();
	$sql 			= "UPDATE user SET password=:password WHERE email=:email";
	$sha_password 	= sha1($password);
	$email_exists	= checkEmail($email, $this->db);

	try {
		if ($email_exists==0) {
			//no existe el correo
			$http_response 			= 500;
			$data["error"] 			= 1;
			$data["description"] 	= "No existe este correo";

		}else{
			//el correo si existe
			$sth = $this->db->prepare($sql);
			$sth->bindParam("password", $sha_password);
			$sth->bindParam("email", $email);
			$sth->execute();
			sendEmail($email, $password);
			$data["error"]			= 0;
			$data["description"]	= "Password reseteado con éxito";
		}

	} catch (PDOException $e) {
		$http_response 			= 500;
		$data["error"] 			= 2;
		$data["description"] 	= $e->getMessage();
	}

	return $this->response->withJson($data, $http_response); 
});

$app->put('/password/[{id_user}]', function ($request, $response, $args){

	$data 			= array();
	$input 			= $request->getParsedBody();
	$http_response 	= 200;
	$password 		= $input['password'];
	$sha_password 	= sha1($password);
	$user 			= $args['id_user'];
	$sql 			= "UPDATE user SET password=:password WHERE id_user=:id_user";
	try {
		$sth = $this->db->prepare($sql);
		$sth->bindParam("password", $sha_password);
		$sth->bindParam("id_user", $user);
		$sth->execute();
		$data["error"]			= 0;
		$data["description"]	= "Password cambiado con éxito";
		
	} catch (PDOException $e) {
		$http_response 			= 500;
		$data["error"] 			= 1;
		$data["description"] 	= $e->getMessage();
	}

	return $this->response->withJson($data, $http_response); 
});


