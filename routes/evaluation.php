<?php

$app->get('/evaluation/user/[{id_user}]', function ($request, $response, $args) {
	
	$data = array();
	$utf = [];
	$http_response = 200;
	$sth = $this->db->prepare("SELECT e.id_evaluation, e.name FROM planification_user pu INNER JOIN evaluation e ON e.id_planification=pu.id_planification WHERE id_user=:id_user");
	try{
		$sth->bindParam("id_user", $args['id_user']);
		$sth->execute();
		$d = $sth->fetchAll();
		foreach ($d as $k) {
			$k['name'] = utf8_encode($k['name']);
			$utf[] = $k;
		}
		$data = array(
			'error' => 0,
			'evaluations' => $utf
		);
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response); 

});


$app->get('/evaluation/[{id_evaluation}]', function ($request, $response, $args) {
	
	$data = array();
	$utf = [];
	$http_response = 200;
	$sth = $this->db->prepare("SELECT * FROM question_evaluation WHERE id_evaluation=:id_evaluation");
	try{
		$sth->bindParam("id_evaluation", $args['id_evaluation']);
		$sth->execute();
		$d = $sth->fetchAll();
		foreach ($d as $k) {
			$k['question'] = utf8_encode($k['question']);
			$utf[] = $k;
		}
		$data = array(
			'error' => 0,
			'questions' => $utf
		);
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response); 

});

