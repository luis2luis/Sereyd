<?php

	$app->get('/resource', function ($request, $response, $args) {
		
		$data = array();
		$http_response = 200;
		$sth = $this->db->prepare("SELECT name, id_category_resource as id_resource FROM `category_resource`");
		try{
			$sth->execute();
			$resources = $sth->fetchAll();
			$data["error"] = 0;
			$data["description"] = "Recursos encontrados con éxito";
			$resourcs = [];
			foreach ($resources as $key) {
				$key["name"] = utf8_encode($key["name"]);
				$resourcs[] = $key;
			}
			$data["resources"] = $resourcs;
		}catch(PDOException $e){
			$data["error"] = 1;
			$http_response = 500;
			$data["description"] = $e->getMessage();
		}
		return $this->response->withJson($data, $http_response);
	});

	
	$app->get('/resource/[{id_resource}]', function ($request, $response, $args) {
		
		$data = array();
		$http_response = 200;
		$sth = $this->db->prepare("SELECT id_resource as id_resource_file, name, url FROM `resource` WHERE id_category_resource=:id_category_resource");
		try{
			$sth->bindParam("id_category_resource", $args["id_resource"]);
			$sth->execute();
			$resources = $sth->fetchAll();
			$data["error"] = 0;
			$data["description"] = "Recursos encontrados con éxito";
			$resourcs = [];
			foreach ($resources as $key) {
				$key["name"] = utf8_encode($key["name"]);
				$resourcs[] = $key;
			}
			$data["resources"] = $resourcs;
		}catch(PDOException $e){
			$data["error"] = 1;
			$http_response = 500;
			$data["description"] = $e->getMessage();
		}
		return $this->response->withJson($data, $http_response); 
	});

