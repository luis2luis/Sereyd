<?php


$app->get('/dashboard/student/[{id_user}]', function($request, $response, $args) {

  $id_user        = $args["id_user"];
  $http_response  = 200;
  $data           = array();
  try {
    $data["students"] = getFullStudentsByUser($id_user, $this->db);

  } catch (PDOException $e) {
    $http_response  = 500;
  }
  return $this->response->withJson($data, $http_response);

});

$app->get('/dashboard/[{id_user}]', function ($request, $response, $args) {

  $id_user        = $args["id_user"];
  $http_response  = 200;
  $data           = array();
  $discussions    = array();
  try {
    $data["count_groups"]     = getCountGroups($id_user, $this->db);
    $data["count_students"]   = getCountStudents($id_user, $this->db);
    $data["user"]             = getUserData($id_user, $this->db);
    $discussions              =  getDiscussions($id_user, $this->db);
    for ($i=0; $i<count($discussions) ; $i++) {
      $discussions[$i]["answer_count"] = count(getAnswersByDiscussion($discussions[$i]["id_discussion"], $this->db));
    }
    $data["forum"]            = $discussions;
  } catch (PDOException $e) {
    $http_response  = 500;
    $data["error"]  = $e->getMessage();
  }

  return $this->response->withJson($data, $http_response);

});

function getCountStudents($id_user, $db){

  $sql  = "SELECT count(s.name) as count FROM student s INNER JOIN `group` g ON s.id_group = g.id_group WHERE g.id_user=:id_user";
  try {
    $sth  = $db->prepare($sql);
    $sth->bindParam("id_user", $id_user);
  	$sth->execute();
    $object = $sth->fetchObject();
    return $object->count;

  } catch (Exception $e) {
    error_log($e->getMessage());
    return 0;
  }

}

function getCountGroups($id_user, $db){

  $sql  = "SELECT count(*) as count FROM `group` WHERE id_user=:id_user";
  try {
    $sth  = $db->prepare($sql);
    $sth->bindParam("id_user", $id_user);
  	$sth->execute();
    $object = $sth->fetchObject();
    return $object->count;
  } catch (Exception $e) {
    error_log($e->getMessage());
    return 0;
  }
}

function getUserData($id_user, $db){

  $sql  = "SELECT * FROM user WHERE id_user=:id_user";
  try {
    $sth  = $db->prepare($sql);
    $sth->bindParam("id_user", $id_user);
  	$sth->execute();
    $object = $sth->fetchObject();
    return $object;
  } catch (Exception $e) {
    error_log($e->getMessage());
    return 0;
  }
}

function getDiscussions($id_user, $db){

  $sql = "SELECT d.*, u.name, u.image FROM discussion d INNER JOIN user u ON d.id_user=u.id_user WHERE u.id_user!=:id_user LIMIT 10";
  try {
    $sth  = $db->prepare($sql);
    $sth->bindParam("id_user", $id_user);
  	$sth->execute();
    $object = $sth->fetchAll();
    return $object;
  } catch (Exception $e) {
    return 0;
  }

}

function getFullStudentsByUser($id_user, $db){

  $sql = " SELECT * FROM student s INNER JOIN `group` g  ON s.id_group=g.id_group WHERE g.id_user=:id_user";
  try {
    $sth  = $db->prepare($sql);
    $sth->bindParam("id_user", $id_user);
  	$sth->execute();
    $object = $sth->fetchAll();
    return $object;
  } catch (Exception $e) {
    error_log($e->getMessage());
    return [];
  }

}
