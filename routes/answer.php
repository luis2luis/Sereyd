<?php

$app->post('/answer', function ($request, $response, $args) {

  $input 					= $request->getParsedBody();
  $answer         = $input["answer"];
  $id_user        = $input["id_user"];
  $id_discussion  = $input["id_discussion"];
  $date           = date("Y-m-d H:i:s");
  $http_response 	= 200;
  $sql 						= "INSERT INTO answer (answer, date_creation, id_user, id_discussion) VALUES (:answer, :date_creation, :id_user, :id_discussion)";

  try {
    $sth = $this->db->prepare($sql);
    $sth->bindParam("answer", $answer);
    $sth->bindParam("id_discussion", $id_discussion);
    $sth->bindParam("date_creation", $date);
    $sth->bindParam("id_user", $id_user);
    $sth->execute();
    $input["id_answer"] = $this->db->lastInsertId();
    $input["date_creation"] = $date;
    if ($input["id_answer"] != false) {
      $data["error"]        = 0;
      $data["description"]  = "Respuesta creada con éxito";
      $data["answer"]   = $input;
    }else{
      $data["error"]       = 1;
      $http_response 	     = 500;
      $data["description"] = "Error al crear respuesta";
    }
  } catch (PDOException $e) {
    $data["error"]        = 1;
    $http_response 	      = 500;
    $data["description"]  = $e->getMessage();
  }
  return $this->response->withJson($data, $http_response);

});
