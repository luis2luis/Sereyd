<?php

$app->post('/student/', function ($request, $response) {

	$data = array();
	$http_response = 200;
	$input = $request->getParsedBody();
	$image = "/assets/emojis/".rand(1, 30).".png";
	$sql = "INSERT INTO `student` (name, lastname, birthday, gender, image, emergency_contact, id_group, email, password) VALUES (:name, :lastname, :birthday, :gender, :image, :emergency_contact, :id_group, :email, :password) ";
	$sth = $this->db->prepare($sql);
	try{
		$sth->bindParam("name", $input['name']);
		$sth->bindParam("lastname", $input['lastname']);
		$sth->bindParam("birthday", $input['birthday']);
		$sth->bindParam("image", $image);
		$sth->bindParam("gender", $input['gender'], PDO::PARAM_INT);
		$sth->bindParam("emergency_contact", $input['emergency_contact']);
		$sth->bindParam("id_group", $input['id_group'], PDO::PARAM_INT);

		if($input['email'] != null){
			$sth->bindParam("email", $input['email']);
			$pass = randomPassword();
			$sth->bindParam("password", sha1($pass));
			sendEmail($input['email'], $pass);
		}else{
			$sth->bindParam("email", $input['email']);
			$sth->bindParam("password", $input['password']);
		}

		$sth->execute();
		$input["image"] = $image;
		$input['id_student'] = $this->db->lastInsertId();
		if($input['id_student'] != false){
			$data["error"] = 0;
			$data["description"] = "Alumno creado con éxito";
			$data["student"] = $input;
		}else{
			$data["error"] = 1;
			$data["description"] = "Error al crear un alumno nuevo";
		}
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response);

});

$app->get('/student/information/[{id_student}]', function($request, $response, $args) {

	$data = array();
	$total_diagnostic = 0;
	$http_response = 200;
	$id_user = $args['id_student'];
	try{
		$attendance = getAttendances($id_user, $this->db);
		$behavior	= getBehaviors($id_user, $this->db);
		
		$data["error"] = 0;
		if($attendance != -1){
			$data["attendance"] = $attendance;
		}

		if($behavior["behavior"] != -1){
			$data["behavior"] = $behavior;
		}
		$language = getDiagnosticLanguage($id_user, $this->db);
		$data["language"] = $language["total"];
		$total_diagnostic+= intval($language["total"]);

		$math = getDiagnosticMath($id_user, $this->db);
		$data["math"] = $math["total"];
		$total_diagnostic+= intval($math["total"]);

		$world = getDiagnosticWorld($id_user, $this->db);
		$data["world"] = $world["total"];
		$total_diagnostic+= intval($world["total"]);

		$health = getDiagnosticHealth($id_user, $this->db);
		$data["health"] = $health["total"];
		$total_diagnostic+= intval($healt["total"]);

		$social = getDiagnosticSocial($id_user, $this->db);
		$data["social"] = $social["total"];
		$total_diagnostic+= intval($social["total"]);

		$art = getDiagnosticArt($id_user, $this->db);
		$data["art"] = $art["total"];
		$total_diagnostic+= intval($art["total"]);

		$attendancesTotal = getAttendancesTotal($id_user, $this->db);
		$attendancesPositive = getAttendancesPositive($id_user, $this->db);
		$attendancePercent = ($attendancesPositive*100)/$attendancesTotal;
		
		$data["diagnostic"] = getDiagnostic($total_diagnostic);
		$data["school_performance"] = getPerformance($data["diagnostic"], $attendancePercent);

	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response); 
});

$app->get('/student/[{id_student}]', function ($request, $response, $args) {
	
	$data = array();
	$http_response = 200;
	$sth = $this->db->prepare("SELECT * FROM `student` WHERE id_student=:id_student");
	try{
		$sth->bindParam("id_student", $args['id_student']);
		$sth->execute();
		$user = $sth->fetchObject();
		$data["error"] = 0;
		$data["description"] = "Alumno encontrado con éxito";
		$data["user"] = $user;
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response); 
});



$app->delete('/student/[{id_student}]', function($request, $response, $args){
	$data = array();
	$sql = "DELETE FROM `student` WHERE id_student=:id_student";
	$sth = $this->db->prepare($sql);
	try{
		$sth->bindParam("id_student", $args['id_student']);
		$exists = studentExists($args['id_student'], $this->db);
		//$sth->execute();

		if(intval($exists->contador) <= 0){
			$data["error"] = 1;
			$http_response = 500;
			if(intval($exists->contador) == 0)
				$data["description"] = "No se encontró un estudiante con este identificador: ".$args['id_student'];
			if(intval($exists->contador) == -1)
				$data["description"] = "Ha ocurrido un error";
		}else{
			$b = deleteAllDataFromStudent($args['id_student'], $this->db);
			$student = $sth->execute();
			$data["error"] = 0;
			$data["description"] = "Estudiante eliminado con éxito";
			$data["student"] = $student;
		}
	}catch(PDOException $e){
		$data["error"] = 1;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data);
});


$app->put('/student/[{id_student}]', function($request, $response, $args){

	$data = array();
	$input = $request->getParsedBody();
	$http_response = 200;
	$id_student = $args['id_student'];
	$sql = "UPDATE `student` SET  name=:name, lastname=:lastname, birthday=:birthday, gender=:gender, emergency_contact=:emergency_contact, email=:email, password=:password WHERE id_student=:id_student";
	$sth = $this->db->prepare($sql);
	try{

		$sth->bindParam("name", $input['name']);
		$sth->bindParam("lastname", $input['lastname']);
		$sth->bindParam("birthday", $input['birthday']);
		$sth->bindParam("gender", $input['gender']);
		$sth->bindParam("emergency_contact", $input['emergency_contact']);
		$sth->bindParam("email", $input['email']);
		$pass = $input['password'];
		$sth->bindParam("password", sha1($pass));
		$sth->bindParam("id_student", $args['id_student']);
		$exists = existsStudent($id_student, $this->db);
		if($exists["error"]==0){
			if($exists["student_contador"]==0){//no existe el alumno
				$data["error"] = 1;
				$http_response = 500;
				$data["description"] = "No se encontró un alumno con este identificador: ".$args['id_student'];
			}else{
				$student = $sth->execute();
				$data["error"] = 0;
				$data["description"] = "Alumno modificado con éxito";
				$data["student"] = $student;
			}
		}else{
			$data["error"] = 1;
			$data["description"] = $exists["description"];
			$http_response = 500;
		}

	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response); 
});




function existsStudent($id_student, $db){

	$r = array();
	$sql = "SELECT count(*) as contador FROM `student` WHERE id_student=:id_student";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_student", $id_student);
		$sth->execute();
		$c = $sth->fetchObject();
		$r["error"] = 0;
		$r["student_contador"] = $c->contador;
		return $r;
	}catch(PDOException $e){
		$r["error"] = 1;
		$r["description"] = $e->getMessage();
		return $r;
	}

}


function studentExists($id_student, $db){

	$sql = "SELECT count(*) as contador FROM `student` WHERE id_student=:id_student";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_student", $id_student);
		$sth->execute();
		return $sth->fetchObject();
	}catch(PDOException $e){
		return -1;
	}
}


function getAttendances($id_student, $db){

	$sql = "SELECT count(*) as count FROM `attendance` WHERE id_student=:id_student AND value=0";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_student", $id_student);
		$sth->execute();
		return $sth->fetchObject();
	}catch(PDOException $e){
		return -1;
	}
}

function getAttendancesTotal($id_user, $db){

	$sql = "SELECT count(*) as count FROM `attendance` WHERE id_student=:id_student";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_student", $id_student);
		$sth->execute();
		return $sth->fetchObject();
	}catch(PDOException $e){
		return -1;
	}

}

function getAttendancesPositive($id_user, $db){

	$sql = "SELECT count(*) as count FROM `attendance` WHERE id_student=:id_student AND value=1";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_student", $id_student);
		$sth->execute();
		return $sth->fetchObject();
	}catch(PDOException $e){
		return -1;
	}

}


function getBehaviors($id_student, $db){

	$avg=array();
	$sql = "SELECT AVG(value) as contador FROM `behavior` WHERE id_student=:id_student";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_student", $id_student);
		$sth->execute();
		$average = $sth->fetchObject();
		if($average->contador >= 0.0 && $average->contador <=1.0){
			//Malo
			$avg["description"] = "Mala";
		}else if($average->contador > 1 && $average->contador < 1.5){
			//Medio
			$avg["description"] = "Regular";
		} else if($average->contador >= 1.5 && $average->contador <= 2){
			//Bueno
			$avg["description"] = "Buena";
		}
		$avg["average"] = $average->contador;
		return $avg;
	}catch(PDOException $e){
		return -1;
	}

}

function getDiagnosticLanguage($id_student, $db){

	$count_total 	= 0;
	$count_yes 		= 0;

	$sql = "SELECT * from question_student WHERE id_student = :id_student AND (id_test=1 OR id_test=7 OR id_test=13)";
	$answer = array();
	try{
		$sth = $db->prepare($sql);
		$sth->bindParam("id_student", $id_student);
		$sth->execute();
		$a = $sth->fetchAll();
		
		$count_total = count($a);

		foreach ($a as $i) {

			if(intval($i["answer"])==1)
				$count_yes++;
			
		}

		$answer["total"] = ($count_yes*100/$count_total==false) ? 0 :  $count_yes*100/$count_total ;
	}catch(PDOException $e){
		$answer["error"] = 1;
	}
	return $answer;
}

function getDiagnosticMath($id_student, $db){

	$count_total 	= 0;
	$count_yes 		= 0;

	$sql = "SELECT * from question_student WHERE id_student = :id_student AND (id_test=2 OR id_test=8 OR id_test=14)";
	$answer = array();
	try{
		$sth = $db->prepare($sql);
		$sth->bindParam("id_student", $id_student);
		$sth->execute();
		$a = $sth->fetchAll();
		
		$count_total = count($a);

		foreach ($a as $i) {

			if(intval($i["answer"])==1)
				$count_yes++;
			
		}

		$answer["total"] = ($count_yes*100/$count_total==false) ? 0 :  $count_yes*100/$count_total ;
	}catch(PDOException $e){
		$answer["error"] = 1;
	}
	return $answer;
}


function getDiagnosticWorld($id_student, $db){

	$count_total 	= 0;
	$count_yes 		= 0;

	$sql = "SELECT * from question_student WHERE id_student=:id_student AND (id_test=3 OR id_test=9 OR id_test=15)";
	$answer = array();
	try{
		$sth = $db->prepare($sql);
		$sth->bindParam("id_student", $id_student);
		$sth->execute();
		$a = $sth->fetchAll();
		
		$count_total = count($a);

		foreach ($a as $i) {

			if(intval($i["answer"])==1)
				$count_yes++;
			
		}

		$answer["total"] = ($count_yes*100/$count_total==false) ? 0 :  $count_yes*100/$count_total ;
	}catch(PDOException $e){
		$answer["error"] = 1;
	}
	return $answer;
}



function getDiagnosticHealth($id_student, $db){

	$count_total 	= 0;
	$count_yes 		= 0;

	$sql = "SELECT * from question_student WHERE id_student=:id_student AND (id_test=4 OR id_test=10 OR id_test=16)";
	$answer = array();
	try{
		$sth = $db->prepare($sql);
		$sth->bindParam("id_student", $id_student);
		$sth->execute();
		$a = $sth->fetchAll();
		
		$count_total = count($a);

		foreach ($a as $i) {

			if(intval($i["answer"])==1)
				$count_yes++;
			
		}

		$answer["total"] = ($count_yes*100/$count_total==false) ? 0 :  $count_yes*100/$count_total ;
	}catch(PDOException $e){
		$answer["error"] = 1;
	}
	return $answer;
}



function getDiagnosticSocial($id_student, $db){

	$count_total 	= 0;
	$count_yes 		= 0;

	$sql = "SELECT * from question_student WHERE id_student=:id_student AND (id_test=5 OR id_test=11 OR id_test=17)";
	$answer = array();
	try{
		$sth = $db->prepare($sql);
		$sth->bindParam("id_student", $id_student);
		$sth->execute();
		$a = $sth->fetchAll();
		
		$count_total = count($a);

		foreach ($a as $i) {

			if(intval($i["answer"])==1)
				$count_yes++;
			
		}

		$answer["total"] = ($count_yes*100/$count_total==false) ? 0 :  $count_yes*100/$count_total ;
	}catch(PDOException $e){
		$answer["error"] = 1;
	}
	return $answer;
}



function getDiagnosticArt($id_student, $db){

	$count_total 	= 0;
	$count_yes 		= 0;

	$sql = "SELECT * from question_student WHERE id_student=:id_student AND (id_test=6 OR id_test=12 OR id_test=18)";
	$answer = array();
	try{
		$sth = $db->prepare($sql);
		$sth->bindParam("id_student", $id_student);
		$sth->execute();
		$a = $sth->fetchAll();
		
		$count_total = count($a);

		foreach ($a as $i) {

			if(intval($i["answer"])==1)
				$count_yes++;
		}

		$answer["total"] = ($count_yes*100/$count_total==false) ? 0 :  $count_yes*100/$count_total ;
	}catch(PDOException $e){
		$answer["error"] = 1;
	}
	return $answer;
}

function deleteAllDataFromStudent($id_student, $db){
	$sql = "DELETE FROM question_student WHERE id_student=:id_student;DELETE FROM question_evaluation_student WHERE id_student=:id_student;DELETE FROM behavior WHERE id_student=:id_student;DELETE FROM attendance WHERE id_student=:id_student;";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_student", $id_student);
		$band = $sth->execute();
		return $band;
	}catch(PDOException $e){
		return -1;
	}
}

function deleteAllDataFromStudentQuestion($id_student, $db){
	$sql = "DELETE FROM question_student WHERE id_student=:id_student";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_student", $id_student);
		$band = $sth->execute();
		return $band;
	}catch(PDOException $e){
		return -1;
	}
	//DELETE FROM question_evaluation_student WHERE id_student=:id_student;DELETE FROM behavior WHERE id_student=:id_student;DELETE FROM attendance WHERE id_student=:id_student;
}

function deleteAllDataFromStudentQuestionEvaluation($id_student, $db){
	$sql = "DELETE FROM question_evaluation_student WHERE id_student=:id_student";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_student", $id_student);
		$band = $sth->execute();
		return $band;
	}catch(PDOException $e){
		return -1;
	}
	//DELETE FROM behavior WHERE id_student=:id_student;DELETE FROM attendance WHERE id_student=:id_student;
}

function deleteAllDataFromStudentBehavior($id_student, $db){
	$sql = "DELETE FROM behavior WHERE id_student=:id_student";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_student", $id_student);
		$band = $sth->execute();
		return $band;
	}catch(PDOException $e){
		return -1;
	}
	//DELETE FROM attendance WHERE id_student=:id_student;
}

function deleteAllDataFromStudentAttendance($id_student, $db){
	$sql = "DELETE FROM attendance WHERE id_student=:id_student";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_student", $id_student);
		$band = $sth->execute();
		return $band;
	}catch(PDOException $e){
		return -1;
	}
}

function getDiagnostic($total){

	return $total/6;

}


function getPerformance($attendance, $diagnostic){

	return (($attendance/2)+($diagnostic/2));

}

