<?php

$app->post('/question_student', function ($request, $response, $args) {
	
	//$sth = $this->db->prepare("SELECT id_user, name, image, id_facebook, email, image FROM user");
	//$sth->execute();
	$data = array();
	$data_tests = array();
	$http_response = 200;
	try{
		$input 		= $request->getParsedBody();
		$answers 	= $input["answers"];
		$id_student = $input["id_student"];
		$id_group_diagnostic = $input["id_group_diagnostic"]; 

		$answers_r = [];

		$id_diagnostic = getDiagnosticById($id_group_diagnostic, $this->db);

		$data_counts =  array(
					array(0,0,0),
					array(0,0,0),
					array(0,0,0),
					array(0,0,0),
					array(0,0,0),
					array(0,0,0)
 				);

		switch ($id_diagnostic) {
			case 1:
				$data_tests = [1,2,3,4,5,6];
				break;
			case 2:
				$data_tests = [13,14,15,16,17,18];
				break;
			case 3:
				$data_tests = [7,8,9,10,11,12];
				break;
			case 4:
				$data_tests = [19, 20,21,22,23,24];
				break;
			default:
				break;
		}
		
		foreach ($answers as $key) {
			
			$key["id_student"] = $id_student;
			//$key["id_test"] = $id_test;
			$key["id_group_diagnostic"] = $id_group_diagnostic;
			$key["success"] = insertAnswer($key, $this->db);

			$answers_r[] = $key;

			//update_data($id_test, $data_counts);

			switch ($key["id_test"]) {
				case $data_tests[0]:
					switch($key["answer"]){
						case 0:
							$data_counts[0][0]++;
							break;
						case 1:
							$data_counts[0][1]++;
							break;
						case 2:
							$data_counts[0][2]++;
							break;
						default:			
							break;
					}
					break;
				case $data_tests[1]:
					switch($key["answer"]){
						case 0:
							$data_counts[1][0]++;
							break;
						case 1:
							$data_counts[1][1]++;
							break;
						case 2:
							$data_counts[1][2]++;
							break;
						default:			
							break;
					}
					break;
				case $data_tests[2]:
					switch($key["answer"]){
						case 0:
							$data_counts[2][0]++;
							break;
						case 1:
							$data_counts[2][1]++;
							break;
						case 2:
							$data_counts[2][2]++;
							break;
						default:			
							break;
					}
					break;
				case $data_tests[3]:
					switch($key["answer"]){
						case 0:
							$data_counts[3][0]++;
							break;
						case 1:
							$data_counts[3][1]++;
							break;
						case 2:
							$data_counts[3][2]++;
							break;
						default:			
							break;
					}
					break;
				case $data_tests[4]:
					switch($key["answer"]){
						case 0:
							$data_counts[4][0]++;
							break;
						case 1:
							$data_counts[4][1]++;
							break;
						case 2:
							$data_counts[4][2]++;
							break;
						default:			
							break;
					}
					break;
				case $data_tests[5]:
					switch($key["answer"]){
						case 0:
							$data_counts[5][0]++;
							break;
						case 1:
							$data_counts[5][1]++;
							break;
						case 2:
							$data_counts[5][2]++;
							break;
						default:			
							break;
					}
					break;
				default:
					break;
			}
		}



		$diagnostics = array();

		for ($m=0; $m < count($data_tests); $m++) { 
			

			$percents = getPercent($data_counts[$m]);
			$diagnostics[$m] = array(
					'name' => utf8_encode(getNameById($data_tests[$m], $this->db)),
					'level' => $percents["level"],
					'percent' => $percents["percent"]
				);

		}


		$data["error"] = 0;
		$data["answers"] = $answers_r;
		$data["answer_count"] = $data_counts;
		$data["final_diagnostics"] = $diagnostics;

	}catch(PDOException $e){

		$http_response = 500;
		$data["error"] = 1;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response);
});



$app->post('/question_student/evaluation/', function ($request, $response, $args) {
	
	$data = array();
	$answers_r = [];
	$data_tests = array();
	$http_response = 200;

	try{
		$input 					= $request->getParsedBody();
		$answers 				= $input["answers"];
		$id_student 			= $input["id_student"];
		$id_group_evaluation 	= $input["id_group_evaluation"]; 
		$id_evaluation 			= getEvaluationById($id_group_evaluation, $this->db);

		$count_yes 			= 0;
		$count_no 			= 0;
		$count_in_process 	= 0;
		
		foreach ($answers as $key) {
			
			$key["id_student"] 			= $id_student;
			$key["id_group_evaluation"] = $id_group_evaluation;
			$key["success"] 			= insertAnswerEvaluation($key, $this->db);

			$answers_r[] = $key;

			switch ($key["answer"]) {
				case 0:
					$count_no++;
					break;
				case 1:
					$count_yes++;
					break;
				case 2:
					$count_in_process++;
					break;
				default:
					break;
			}

		}



		/*$diagnostics = array();

		for ($m=0; $m < count($data_tests); $m++) { 
			

			$percents = getPercent($data_counts[$m]);
			$diagnostics[$m] = array(
					'name' => utf8_encode(getNameById($data_tests[$m], $this->db)),
					'level' => $percents["level"],
					'percent' => $percents["percent"]
				);

		}*/


		$data["error"] = 0;
		$data["answers"] = $answers_r;
		$data["answers_yes"] = $count_yes;
		$data["answers_no"] = $count_no;
		$data["answers_in_process"] = $count_in_process;

		//$data["answer_count"] 		= $data_counts;
		//$data["final_diagnostics"] 	= $diagnostics;

	}catch(PDOException $e){

		$http_response = 500;
		$data["error"] = 1;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response);

});

function getPercent($array_counts){

	$no_count = $array_counts[0];
	$yes_count = $array_counts[1];
	$in_process_count  = $array_counts[2];

	$total = $no_count+$yes_count+$in_process_count;

	$percent = ($yes_count * 100) / $total;

	if($percent > 0 && $percent <= 33)
		$level = "Nivel bajo";
	if($percent > 33 && $percent <= 66)
		$level = "Nivel medio";
	if($percent > 66 && $percent <= 100)
		$level = "Nivel alto";

	$data = array();
	$data["level"] = $level;
	$data["percent"] = $percent;

	return $data;
}


function insertAnswer($data_answer, $db){

	$sql = "INSERT INTO question_student (answer, id_question, id_student, id_group_diagnostic, id_test) VALUES (:answer, :id_question, :id_student, :id_group_diagnostic, :id_test)";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("answer", $data_answer["answer"]);
		$sth->bindParam("id_question", $data_answer["id_question"]);
		$sth->bindParam("id_student", $data_answer["id_student"]);
		$sth->bindParam("id_test", $data_answer["id_test"]);
		$sth->bindParam("id_group_diagnostic", $data_answer["id_group_diagnostic"]);
		$sth->execute();
		return true;
	}catch(PDOException $e){
		return false;
	}

}

function insertAnswerEvaluation($data_answer, $db){

	$sql = "INSERT INTO question_evaluation_student (answer, id_question_evaluation, id_student, id_group_evaluation) VALUES (:answer, :id_question_evaluation, :id_student, :id_group_evaluation)";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("answer", $data_answer["answer"]);
		$sth->bindParam("id_question_evaluation", $data_answer["question_evaluation"]);
		$sth->bindParam("id_student", $data_answer["id_student"]);
		$sth->bindParam("id_group_evaluation", $data_answer["id_group_evaluation"]);
		$sth->execute();
		return true;
	}catch(PDOException $e){
		return false;
	}

}

function getNameById($id_test, $db){

	$sql = "SELECT name FROM test WHERE id_test=:id_test";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_test", $id_test);
		$sth->execute();
		$obj = $sth->fetchObject();
		return $obj->name;
	}catch(PDOException $e){
		return false;
	}

}

function getDiagnosticById($id_group_diagnostic, $db){

	$sql = "SELECT id_diagnostic as id FROM group_diagnostic WHERE id_group_diagnostic=:id_group_diagnostic";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_group_diagnostic", $id_group_diagnostic);
		$sth->execute();
		$obj = $sth->fetchObject();
		return $obj->id;
	}catch(PDOException $e){
		return false;
	}
}

function getEvaluationById($id_group_evaluation, $db){

	$sql = "SELECT id_evaluation as id FROM group_evaluation WHERE id_group_evaluation=:id_group_evaluation";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_group_evaluation", $id_group_evaluation);
		$sth->execute();
		$obj = $sth->fetchObject();
		return $obj->id;
	}catch(PDOException $e){
		return false;
	}
}