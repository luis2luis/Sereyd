<?php


$app->get('/subscription', function ($request, $response, $args) {


  try {
    Openpay::setId(MERCHANT_ID);
    Openpay::setApiKey(PRIVATE_KEY);
    $openpay 		= Openpay::getInstance(MERCHANT_ID, PRIVATE_KEY);

    $findDataRequest = array(
      'limit' => 20
    );

    /*$customerList = $openpay->customers->getList($findDataRequest);

      for ($i=0 ; $i < count($customerList)  ; $i++ ) {
        error_log($customerList[$i]->id);
        $id       = $customerList[$i]->id;
        $customer = $openpay->customers->get($id);
        $customer->delete();
      }

      /*$plan = $openpay->plans->get('p1uzlxjveyrrehvpjyfy');
      $plan->trial_days = 0;
      $plan->save();*/

  } catch (Exception $e) {
    error_log($e->getMessage());
  }



});



$app->post('/subscription/cancel/[{id_user}]', function ($request, $response, $args) {

  $id_user        = $args["id_user"];
  $http_response	= 500;
  $data 					= array();
  $data["error"]  = 1;
  try {
    Openpay::setId(MERCHANT_ID);
    Openpay::setApiKey(PRIVATE_KEY);
    $openpay 		= Openpay::getInstance(MERCHANT_ID, PRIVATE_KEY);
    //Openpay::setProductionMode(true); //producción

    $cancelOpenpay = cancelSubscriptionOpenpay($id_user, $this->db, $openpay);

    if($cancelOpenpay["error"]==1){
      //ocurrio un error en la transaccion
    }else{
      //sin errores
      $cancelAll = deleteAllPay($id_user, $this->db);
      if($cancelAll != -1){
        $http_response			= 200;
    		$data["error"]			= 0;
    		$data["desc"]			  = "Suscripción del usuario cancelada con éxito";
      }
    }

  }catch (OpenpayApiTransactionError $e) {
	$data["description"] 	= 'ERROR on the transaction: ' . $e->getMessage() .
	      ' [error code: ' . $e->getErrorCode() .
	      ', error category: ' . $e->getCategory() .
	      ', HTTP code: '. $e->getHttpCode() .
	      ', request ID: ' . $e->getRequestId() . ']';
	} catch (OpenpayApiRequestError $e) {
		$data["description"] 	= 'ERROR on the request: ' . $e->getMessage();
	} catch (OpenpayApiConnectionError $e) {
		$data["description"] 	= 'ERROR while connecting to the API: ' . $e->getMessage();
	} catch (OpenpayApiAuthError $e) {
		$data["description"] 	= 'ERROR on the authentication: ' . $e->getMessage();
	} catch (OpenpayApiError $e) {
		$data["description"] 	= 'ERROR on the API: ' . $e->getMessage();
	} catch (Exception $e) {
		$data["description"] 	= 'Error on the script: ' . $e->getMessage();
	} catch (PDOException $e) {
		$data["description"] 	= 'Error on the DB: ' . $e->getMessage();
	}

	return $this->response->withJson($data, $http_response);

});


function cancelSubscriptionOpenpay($id_user, $db, $openpay){

  $sql 	= "SELECT p.*, u.id_openpay as id_openpay_user FROM user u INNER JOIN pay p ON u.id_user= p.id_user WHERE p.id_user=:id_user AND active=1 ORDER BY p.date DESC LIMIT 1";
	$sth 	= $db->prepare($sql);
	$r 		= array();
	try {
		$sth->bindParam("id_user", $id_user);
		$sth->execute();
		$dataOP = $sth->fetchObject();
		$id 								= $dataOP->id_openpay;
		$id_openpay_user 		= $dataOP->id_openpay_user;
		if(strlen($id)<=0){
			//el id esta vacio
			$r["error"] 					= 1;
		}else{
			//el id no esta vacio
			$customer 					= $openpay->customers->get($id_openpay_user);
			$subscription 			= $customer->subscriptions->get($id);
      $subscription->delete();
      $r["error"]         = 0;
		}
	} catch (Exception $e) {
		$r["error"] = 1;
		$r["description"] = "Error obteniendo el contador de pagos ".$e->getMessage();
	}

	return $r;

}
