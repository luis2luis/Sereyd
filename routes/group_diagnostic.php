<?php

$app->post('/group_diagnostic', function ($request, $response, $args) {

	$data = array();
	$http_response = 200;
	$fecha = date('Y-m-d');
	$input = $request->getParsedBody();
	$sql = "INSERT INTO group_diagnostic (id_diagnostic, id_group, created_at) VALUES (:id_diagnostic, :id_group, :created_at)";
	$sth = $this->db->prepare($sql);
	try{
		$sth->bindParam("id_diagnostic", $input['id_diagnostic']);
		$sth->bindParam("id_group", $input['id_group']);
		$sth->bindParam("created_at", $fecha);
		$sth->execute();
		$data["error"] = 0;
		$data["description"] = "Grupo y diagnóstico asociados con éxito";
		$input["completed"] = 0;
		$input["created_at"] = $fecha;
		$input["id_group_diagnostic"] = $this->db->lastInsertId();
		$data["group_diagnostic"] = $input;
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}

	return $this->response->withJson($data, $http_response);

});

$app->get('/group_diagnostic/[{id_group_diagnostic}]', function($request, $response, $args){

	$data = array();
	$http_response = 200;
	$id_group_diagnostic = $args["id_group_diagnostic"];

	$id_group = getGroupById_($id_group_diagnostic, $this->db);
	//SELECT id_student FROM student WHERE id_group = :id_group;
	//primero saco todos los alumnos de un grupo
	$studentsOnGroup = getAllStudentsOnGroup($id_group, $this->db);
	$students_in_group = [];
	foreach ($studentsOnGroup as $k) {
		$students_in_group[] = $k["id_student"];
	}

	//SELECT id_student FROM question_student WHERE id_group_diagnostic=:id_group_diagnostic group by id_student;
	//despues obtengo los alumnos que ya ha sido calificados en este group_diagnostic
	$studentsOnDiagnostic = getAllStudentsOnDiagnostic($id_group_diagnostic, $this->db);
	$students_in_diagnostic = [];
	foreach ($studentsOnDiagnostic as $ki) {
		$students_in_diagnostic[] = $ki["id_student"];
	}
	//por ultimo hago una resta del primer array obtenido menos el segundo
	$students_pending = [];
	for ($i=0; $i < count($students_in_group); $i++){

		$b = array_search($students_in_group[$i], $students_in_diagnostic);
		if($b===false){
			array_push($students_pending,$students_in_group[$i]);
		}
	}



	$data["error"] = 0;
	$data["id_group"] = $id_group;
	$data["students_in_group"] = $students_in_group;
	$data["students_completed"] = $students_in_diagnostic;
	$data["students_pending"] = $students_pending;


	return $this->response->withJson($data, $http_response);

});

$app->get('/group_diagnostic/final/[{id_group_diagnostic}]', function($request, $response, $args){

	$data = array();
	$http_response = 200;
	$input = $request->getParsedBody();
	$id_group_diagnostic = $args["id_group_diagnostic"];

	$id_diagnostic = getDiagnosticById_($id_group_diagnostic, $this->db);

	$data_counts =  array(
		array(0,0,0),
		array(0,0,0),
		array(0,0,0),
		array(0,0,0),
		array(0,0,0),
		array(0,0,0)
	);

	switch ($id_diagnostic) {
		case 1:
			$data_tests = [1,2,3,4,5,6];
			break;
		case 2:
			$data_tests = [13,14,15,16,17,18];
			break;
		case 3:
			$data_tests = [7,8,9,10,11,12];
			break;
		case 4:
			$data_tests = [19, 20,21,22,23,24];
			break;
		default:
			break;
	}

	$ans 		= getAllAnswers($id_group_diagnostic, $this->db);
	$answers 	= $ans["answers"];

	foreach ($answers as $key) {

		switch ($key["id_test"]) {
				case $data_tests[0]:
					switch($key["answer"]){
						case 0:
							$data_counts[0][0]++;
							break;
						case 1:
							$data_counts[0][1]++;
							break;
						case 2:
							$data_counts[0][2]++;
							break;
						default:
							break;
					}
					break;
				case $data_tests[1]:
					switch($key["answer"]){
						case 0:
							$data_counts[1][0]++;
							break;
						case 1:
							$data_counts[1][1]++;
							break;
						case 2:
							$data_counts[1][2]++;
							break;
						default:
							break;
					}
					break;
				case $data_tests[2]:
					switch($key["answer"]){
						case 0:
							$data_counts[2][0]++;
							break;
						case 1:
							$data_counts[2][1]++;
							break;
						case 2:
							$data_counts[2][2]++;
							break;
						default:
							break;
					}
					break;
				case $data_tests[3]:
					switch($key["answer"]){
						case 0:
							$data_counts[3][0]++;
							break;
						case 1:
							$data_counts[3][1]++;
							break;
						case 2:
							$data_counts[3][2]++;
							break;
						default:
							break;
					}
					break;
				case $data_tests[4]:
					switch($key["answer"]){
						case 0:
							$data_counts[4][0]++;
							break;
						case 1:
							$data_counts[4][1]++;
							break;
						case 2:
							$data_counts[4][2]++;
							break;
						default:
							break;
					}
					break;
				case $data_tests[5]:
					switch($key["answer"]){
						case 0:
							$data_counts[5][0]++;
							break;
						case 1:
							$data_counts[5][1]++;
							break;
						case 2:
							$data_counts[5][2]++;
							break;
						default:
							break;
					}
					break;
				default:
					break;
			}

	}

	$diagnostics = array();

	for ($m=0; $m < count($data_tests); $m++) {

		$percents = getPercent_($data_counts[$m]);
		$diagnostics[$m] = array(
				'name' => utf8_encode(getNameById_($data_tests[$m], $this->db)),
				'level' => $percents["level"],
				'percent' => $percents["percent"]
			);
	}

	$data["error"] = 0;
	$data["final_diagnostics"] = $diagnostics;

	return $this->response->withJson($data, $http_response);

});

$app->get('/group_diagnostic/pending/[{id_group}]', function($request, $response, $args){

	$data = array();
	$http_response = 200;

	try{
		$sql = "SELECT id_diagnostic as diagnostic, completed, id_group_diagnostic, created_at FROM group_diagnostic WHERE id_group=:id_group";
		$sth = $this->db->prepare($sql);
		$sth->bindParam("id_group", $args["id_group"]);
		$sth->execute();
		$d = $sth->fetchAll();
		$data = array(
			'error' => 0,
			'diagnostics' => $d
		);
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response);
});


$app->put('/group_diagnostic/completed/[{id_group_diagnostic}]', function ($request, $response, $args) {

	$data = array();
	$http_response = 200;
	try{
		$sth = $this->db->prepare("UPDATE group_diagnostic SET completed=1 WHERE id_group_diagnostic = :id_group_diagnostic");
		$sth->bindParam("id_group_diagnostic", $args["id_group_diagnostic"]);
		$band=$sth->execute();
		$data = array(
			'error' => 0,
			'completed' => $band
		);
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response);
});

$app->get('/group_diagnostic/file/[{id_group_diagnostic}]', function ($request, $response, $args) {

	try {
		$data 									= array();
		$http_response 					= 200;
		$id_group_diagnostic 		= $args['id_group_diagnostic'];
		$students 							= getStudentsFromDiagnostic($id_group_diagnostic, $this->db);
		$id_diagnostic 					= getIdDiagnostic($id_group_diagnostic, $this->db);
		$test_ids								= getTestsByIdDiagnostic($id_diagnostic, $this->db);
		$tests 									= array();

		$info_group 						= getInfoGroup($id_group_diagnostic, $this->db);

		foreach ($test_ids as $y)
			$tests[] = $y["id_test"];

		$questions_for_diagnostic 	= getQuestionsForDiagnostic($tests, $this->db);

		$table	= "<table><tr><th>".$info_group["school"]."</th><th>".$info_group["grade"]."</th><th>".$info_group["group_number"]."</th><th>".$info_group["period"]."</th></tr>";
		$table 	.= "<tr><th>No.</th><th>Nombre y Apellido</th>";
	    foreach ($questions_for_diagnostic as $k) {
	    	$table .= "<th colspan='3' style='font-size: 20px;background-color: #000;color:#FFF;'>".$k["question"]."</th>";

	    }
	    $table .= "</tr>";
	    $header = "<td>Si</td><td>No</td><td>En Proceso</td>";
	    $table .= "<tr><td></td><td></td>";

	    for ($i=0; $i < 30; $i++) {
	    	$table .= $header;
	    }

	    $table .= "</tr>";


		$reset 				= 0;
	    $student_count  = 1;
	    $student_actual = "";

		foreach ($students as $z) {

			if(($reset % 30) == 0){
				$student_actual = $z["fullname"];
				$table .= "<tr><td>".$student_count."</td><td>".$student_actual."</td>";
				$table .= answerHTML(intval($z["answer"]));
				$student_count++;
			}else{
				$table .= answerHTML(intval($z["answer"]));
			}
			$reset++;
	   }

	  $tmpfile 				= __DIR__.'/../public/assets/xls/html/'.time().'.html';
		file_put_contents($tmpfile, $table);
		chmod($tmpfile, 777);
		if(file_exists($tmpfile)){
			$objPHPExcel 		= new PHPExcel();


			//Style

			$styleArray = array(
				'font'  => array(
	        'bold'  => true,
	        'color' => array('rgb' => '000000'),
	        'size'  => 12
			));

			$objPHPExcel->getActiveSheet()
			    ->getStyle('A1:CN2')
			    ->getFill()
			    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
			    ->getStartColor()
			    ->setARGB('FDC100'); //#FAE4D3

			$objPHPExcel->getActiveSheet()
					->getStyle('A3:CN3')
					->getFill()
					->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
					->getStartColor()
					->setARGB('FAE4D3');

		 $objPHPExcel->getActiveSheet()->getStyle('A1:CN2')->applyFromArray($styleArray);

		 for ($i=0; $i < (count($students)/30) ; $i++) {
			if( ($i%2)!=0){
				$n = $i+4;
				$objPHPExcel->getActiveSheet()
						->getStyle('A'.$n.':CN'.$n)
						->getFill()
						->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
						->getStartColor()
						->setARGB('FAE4D3');
			}
		}



			//Style


			$excelHTMLReader 	= PHPExcel_IOFactory::createReader('HTML');
			$excelHTMLReader->loadIntoExisting($tmpfile,$objPHPExcel);
			$objWriter 			= new PHPExcel_Writer_Excel2007($objPHPExcel);
			$name_file 			= time().'.xlsx';
			$path 				= '/../public/assets/xls/'.$name_file;
			$objWriter->save(__DIR__.$path);

			$data["error"] 			= 0;
			$data["path"] 			= "/assets/xls/".$name_file;
			$data["path_"] 			= $tmpfile;
			$data["file_exists"] 	= file_exists($tmpfile);

			unlink($tmpfile);

		}else{
			$data["error"] 			= 1;
			$data["path"] 			= $tmpfile;
			$http_response 			= 500;
			$data["file_exists"] 	= file_exists($tmpfile) ;
		}


	} catch (PDOException $e) {
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}

	return $this->response->withJson($data, $http_response);

});

function getTestsByIdDiagnostic($id_diagnostic, $db){

	$sql = "SELECT id_test FROM test WHERE id_diagnostic=:id_diagnostic";
	try {
		$sth = $db->prepare($sql);
		$sth->bindParam("id_diagnostic", $id_diagnostic);
		$sth->execute();
		$c = $sth->fetchAll();
		return $c;
	} catch (PDOException $e) {
		return -1;
	}

}

function getStudentsFromDiagnostic($id_group_diagnostic, $db){


	$sql = "SELECT qes.*, concat(s.name,' ',s.lastname) as fullname FROM question_student qes INNER JOIN student s ON qes.id_student=s.id_student WHERE id_group_diagnostic=:id_group_diagnostic";
	try {
		$sth 	= $db->prepare($sql);
		$sth->bindParam("id_group_diagnostic", $id_group_diagnostic);
		$sth->execute();
		$c 		= $sth->fetchAll();
		return $c;
	} catch (PDOException $e) {
		return -1;
	}
}


function getQuestionsForDiagnostic($tests, $db){

	$in 	= str_repeat('?,', count($tests) - 1) . '?';
	$sql 	= "SELECT * FROM question WHERE id_test IN ($in)";
	try {
		$sth = $db->prepare($sql);
		$sth->execute($tests);
		$c = $sth->fetchAll();
		return $c;
	} catch (PDOException $e) {
		return -1;
	}
}

function getIdDiagnostic($id_group_diagnostic, $db){

	$sql = "SELECT id_diagnostic FROM group_diagnostic WHERE id_group_diagnostic=:id_group_diagnostic";
	try {
		$sth = $db->prepare($sql);
		$sth->bindParam("id_group_diagnostic", $id_group_diagnostic);
		$sth->execute();
		$c = $sth->fetchObject();
		return intval($c->id_diagnostic);
	} catch (PDOException $e) {
		return -1;
	}

}


function getGroupById_($id_group_diagnostic, $db){

	$sql = "SELECT id_group as id FROM group_diagnostic WHERE id_group_diagnostic=:id_group_diagnostic";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_group_diagnostic", $id_group_diagnostic);
		$sth->execute();
		$obj = $sth->fetchObject();
		return $obj->id;
	}catch(PDOException $e){
		return false;
	}
}

function getAllStudentsOnGroup($id_group, $db){

	$sql = "SELECT id_student FROM student WHERE id_group=:id_group;";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_group", $id_group);
		$sth->execute();
		$obj = $sth->fetchAll();
		return $obj;
	}catch(PDOException $e){
		return false;
	}

}

function  getAllStudentsOnDiagnostic($id_group_diagnostic, $db){

	$sql = "SELECT id_student FROM question_student WHERE id_group_diagnostic=:id_group_diagnostic group by id_student";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_group_diagnostic", $id_group_diagnostic);
		$sth->execute();
		$obj = $sth->fetchAll();
		return $obj;
	}catch(PDOException $e){
		return false;
	}


}



function getNameById_($id_test, $db){

	$sql = "SELECT name FROM test WHERE id_test=:id_test";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_test", $id_test);
		$sth->execute();
		$obj = $sth->fetchObject();
		return $obj->name;
	}catch(PDOException $e){
		return false;
	}

}


function getTotal($id_group_diagnostic, $db){

	$data = array();
	try{
		$sql = "SELECT count(*) as count FROM question_student WHERE id_group_diagnostic=:id_group_diagnostic";
		$sth = $db->prepare($sql);
		$sth->bindParam("id_group_diagnostic", $id_group_diagnostic);
		$sth->execute();
		$c = $sth->fetchObject();
		$data = array(
			'error' => 0,
			'count' => $c->count
		);
	}catch(PDOException $e){
		$data["error"] = 1;
		$data["description"] = $e->getMessage();
	}
	return $data;

}


function getPercent_($array_counts){

	$no_count = $array_counts[0];
	$yes_count = $array_counts[1];
	$in_process_count  = $array_counts[2];

	$total = $no_count+$yes_count+$in_process_count;

	$percent = ($yes_count * 100) / $total;

	if($percent >= 0 && $percent <= 33)
		$level = "Nivel bajo";
	if($percent > 33 && $percent <= 66)
		$level = "Nivel medio";
	if($percent > 66 && $percent <= 100)
		$level = "Nivel alto";

	$data = array();
	$data["level"] = $level;
	$data["percent"] = $percent;

	return $data;
}

function getDiagnosticById_($id_group_diagnostic, $db){

	$sql = "SELECT id_diagnostic as id FROM group_diagnostic WHERE id_group_diagnostic=:id_group_diagnostic";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_group_diagnostic", $id_group_diagnostic);
		$sth->execute();
		$obj = $sth->fetchObject();
		return $obj->id;
	}catch(PDOException $e){
		return false;
	}
}



function getAllAnswers($id_group_diagnostic, $db){

	$data = array();
	try{
		$sql = "SELECT * FROM question_student WHERE id_group_diagnostic=:id_group_diagnostic";
		$sth = $db->prepare($sql);
		$sth->bindParam("id_group_diagnostic", $id_group_diagnostic);
		$sth->execute();
		$c = $sth->fetchAll();
		$data = array(
			'error' => 0,
			'answers' => $c
		);
	}catch(PDOException $e){
		$data["error"] = 1;
		$data["description"] = $e->getMessage();
	}
	return $data;

}

function getPositivesAnswer($id_group_diagnostic, $db){

	$data = array();
	try{
		$sql = "SELECT count(answer) as count FROM question_student WHERE id_group_diagnostic=:id_group_diagnostic AND answer=1";
		$sth = $db->prepare($sql);
		$sth->bindParam("id_group_diagnostic", $id_group_diagnostic);
		$sth->execute();
		$c = $sth->fetchObject();
		$data = array(
			'error' => 0,
			'count' => $c->count
		);
	}catch(PDOException $e){
		$data["error"] = 1;
		$data["description"] = $e->getMessage();
	}
	return $data;

}

function getInfoGroup($id_group_diagnostic, $db){

	$data = array();
	try {
		$sql = "SELECT g.* FROM `group` g INNER JOIN group_diagnostic gd ON gd.id_group=g.id_group WHERE gd.id_group_diagnostic=:id_group_diagnostic";
		$sth = $db->prepare($sql);
		$sth->bindParam("id_group_diagnostic", $id_group_diagnostic);
		$sth->execute();
		$c = $sth->fetchObject();
		$data["error"] 				= 0;
		$data["group"] 				= $c;
		$data["school"] 			= $c->school;
		$data["grade"] 				= $c->grade;
		$data["group_number"] = $c->group_number;
		$data["period"] 			= $c->period;
	} catch (PDOException $e) {
		$data["error"] = 1;
		$data["description"] = $e->getMessage();
	}
		return $data;
}
