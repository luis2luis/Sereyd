<?php

$app->get('/planification', function ($request, $response, $args) {
	
	$data = array();
	$http_response = 200;
	$sth = $this->db->prepare("SELECT * FROM planification");
	try{
		$sth->execute();
		$plans = $sth->fetchAll();
		$planifications = [];
		foreach ($plans as $key) {
			$key['name'] = utf8_encode($key['name']);
			$key['formative_field'] = utf8_encode($key['formative_field']);
			$key['description'] =  utf8_encode($key['description']);
			$planifications[] = $key;
		}
		$data = array(
			'error' => 0,
			'planifications' => $planifications
		);
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response); 

});


$app->get('/planification/[{id_planification}]', function ($request, $response, $args) {
	
	$data = array();
	$http_response = 200;
	$sth = $this->db->prepare("SELECT * FROM planification WHERE id_planification=:id_planification");
	try{
		$sth->bindParam("id_planification", $args['id_planification']);
		$sth->execute();
		$plans = $sth->fetchObject();
		$plans->name 			= utf8_encode($plans->name);
		$plans->description 	= utf8_encode($plans->description);
		$data = array(
			'error' => 0,
			'planification' => $plans
		);
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response); 

});


$app->get('/planification/user/[{id_user}]', function ($request, $response, $args) {
	
	$data = array();
	$planifications = [];
	$http_response = 200;
	$sql = "SELECT planification.*, user.id_user FROM planification_user INNER JOIN planification ON planification.id_planification = planification_user.id_planification INNER JOIN user ON user.id_user = planification_user.id_user WHERE planification_user.id_user = :id_user";
	$sth = $this->db->prepare($sql);
	try{
		$sth->bindParam("id_user", $args['id_user']);
		$sth->execute();
		$plans = $sth->fetchAll();
		foreach ($plans as $key) {
			$key['name'] = utf8_encode($key['name']);
			$key['formative_field'] = utf8_encode($key['formative_field']);
			$key['description'] =  utf8_encode($key['description']);
			$planifications[] = $key;
		}
		$data = array(
			'error' => 0,
			'planifications' => $planifications
		);
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response); 

});


