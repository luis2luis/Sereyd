<?php

$app->get('/test', function ($request, $response, $args) {
	
	$data = array();
	$tests = [];
	$http_response = 200;
	$sth = $this->db->prepare("SELECT id_test, name FROM test");
	try{
		$sth->execute();
		$t = $sth->fetchAll();
		foreach ($t as $k ) {
			$k["name"] = utf8_encode($k['name']);
			$tests[] = $k;
		}
		$data = array(
			'error' => 0,
			'tests' => $tests
		);
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response); 

});


$app->get('/test/[{id_diagnostic}]', function ($request, $response, $args) {
	
	$data = array();
	$tests = [];
	$http_response = 200;
	$sth = $this->db->prepare("SELECT id_test, name FROM test WHERE id_diagnostic=:id_diagnostic");
	try{
		$sth->bindParam("id_diagnostic", $args["id_diagnostic"]);
		$sth->execute();
		$t = $sth->fetchAll();
		foreach ($t as $k ) {
			$k["name"] = utf8_encode($k['name']);
			$tests[] = $k;
		}
		$data = array(
			'error' => 0,
			'tests' => $tests
		);
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response); 

});


$app->get('/test/full/[{id_diagnostic}]', function ($request, $response, $args) {
	
	$data = array();
	$tests = [];
	$http_response = 200;
	$sth = $this->db->prepare("SELECT id_test, name FROM test WHERE id_diagnostic=:id_diagnostic");
	try{
		$sth->bindParam("id_diagnostic", $args["id_diagnostic"]);
		$sth->execute();
		$t = $sth->fetchAll();
		foreach ($t as $k ) {
			$k["name"] = utf8_encode($k['name']);
			$tsts = getQuestionsForTest($k["id_test"], $this->db);
			if($tsts->error == 1){
				$k["questions"] = [];
			}else{
				$k["questions"] = $tsts["questions"];
			}
			$tests[] = $k;
			
		}
		$data = array(
			'error' => 0,
			'tests' => $tests
		);
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response); 

});


function getQuestionsForTest($id_test, $db){

	$r = array();
	$p = [];
	$sql = "SELECT id_question, question FROM `question` WHERE id_test=:id_test";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_test", $id_test);
		$sth->execute();
		$c = $sth->fetchAll();
		foreach ($c as $e) {
			$e["question"] = utf8_encode($e["question"]);
			$p[] = $e;
		}
		$r["questions"] = $p;
		return $r;
	}catch(PDOException $e){
		$r["error"] = 1;
		$r["description"] = $e->getMessage();
		return $r;
	}

}