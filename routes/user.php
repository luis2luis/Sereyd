<?php


$app->get('/user', function ($request, $response, $args) {

	$sth = $this->db->prepare("SELECT id_user, name, image, id_facebook, email, image FROM user");
	$sth->execute();
	$users = $sth->fetchAll();
	return $this->response->withJson($users);
});

$app->get('/user/[{id}]', function($request, $response, $args){

	$sth = $this->db->prepare("SELECT id_user, name, image, id_facebook, email, image FROM user WHERE id_user=:id");
	$sth->bindParam("id", $args['id']);
	$sth->execute();
	$user = $sth->fetchObject();
	return $this->response->withJson($user);

});

$app->post('/user/', function($request, $response){

	//email, password, name OR id_facebook

	try{
		$data 					= array();
		$http_response 	= 200;
		$input 					= $request->getParsedBody();
		$email 					= $input['email'];
		$id_facebook 		= $input['id_facebook'];
		$name 					= $input['name'];
		$img 						= "http://res.cloudinary.com/lconder/image/upload/v1487018318/profiletest.jpg";
		$fecha					= date('Y-m-d H:i:s');
		$null 					= null;

		if(($id_facebook==null) || (strlen($id_facebook)<=0)){
			//registro con email
			$password 			= randomPassword();
			$sql 						= "INSERT INTO user (email, password, name, date_creation, image) VALUES (:email, :password, :name, :date_creation, :image)";
			$band_email 		= checkEmail($email, $this->db);
			$sth = $this->db->prepare($sql);
			if($band_email > 0){
				$data["error"] 				= 2;
				$http_response 				= 500;
				$data["description"] 	= "Este email ya esta registrado";
				$data["id_facebook"] 	= ($id_facebook==null);
				$data["id_facebook_l"] 	= (strlen($id_facebook)<=0);
			}else{
				$sth->bindParam("email", $email);
				$sth->bindParam("password", sha1($password));
				$sth->bindParam("name", $name);
				$sth->bindParam("date_creation", $fecha);
				$sth->bindParam("image", $img);
				$sth->execute();
				sendEmail($email, $password);
				$input['id_user'] = $this->db->lastInsertId();
				if($input['id_user'] != false){
					$data["error"] = 0;
					$data["description"] = "Usuario creado con éxito";
					createNewPaymentTrial($input['id_user'], $this->db);
					$data["user"] = $input;
				}else{
					$data["error"] = 1;
					$data["description"] = "Error al crear un usuario nuevo";
				}
			}

		}else{
			//registro con facebook
			$sql = "INSERT INTO user (id_facebook, email, name, date_creation, image) VALUES (:id_facebook, :email, :name, :date_creation, :image)";
			$sth = $this->db->prepare($sql);
			$sth->bindParam("name", $name);
			$sth->bindParam("date_creation", $fecha);
			$sth->bindParam("image", $img);
			$sth->bindParam("id_facebook", $id_facebook);
			if(($email==null) || (strlen(email)<=0))
				$sth->bindParam("email", $null);
			else
				$sth->bindParam("email", $email);
			$sth->execute();
			$input['id_user'] = $this->db->lastInsertId();
			if($input['id_user'] != false){
				$data["error"] = 0;
				$data["description"] = "Usuario creado con éxito";
				createNewPaymentTrial($input['id_user'], $this->db);
				$data["user"] = $input;
			}else{
				$data["error"] = 1;
				$data["description"] = "Error al crear un usuario nuevo";
			}
		}

	}catch(PDOException $e){
		$data["error"] = 1;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response);

});

$app->put('/user/[{id_user}]', function($request, $response, $args){

	$data = array();
	$input = $request->getParsedBody();
	$email = $input['email'];
	$http_response = 200;

	try{
		if(strlen($email)==0){
			//no se modifico el correo
			$sql = "UPDATE user SET name=:name WHERE id_user=:id_user";
			$band_email = 0;
		}else{
			//se modifico el correo
			$sql = "UPDATE user SET name=:name, email=:email WHERE id_user=:id_user";
			$band_email = checkEmail($email, $this->db);
		}
		$sth = $this->db->prepare($sql);
		if($band_email > 0){
			$data["error"] = 2;
			$http_response = 500;
			$data["description"] = "Este email ya esta registrado";
		} else {
			$sth->bindParam("name", $input['name']);
			$sth->bindParam("id_user", $args['id_user']);

			if(strlen($email)!=0)
				$sth->bindParam("email", $input['email']);

			$success = $sth->execute();
			if($success != false){
				$data["error"] = 0;
				$data["description"] = "Usuario modificado con éxito";
				$data["modified"] = true;
			}else{
				$data["error"] = 1;
				$data["description"] = "Error al editar un usuario";
				$http_response = 500;
			}
		}
	}catch(PDOException $e){
		$data["error"] = 1;
		$data["description"] = $e->getMessage();
		$http_response = 500;
	}
	return $this->response->withJson($data, $http_response);
});

$app->post('/user/image/[{id_user}]', function($request, $response, $args){

	$data = array();
	$http_response = 200;
	$files = $request->getUploadedFiles();

	if(empty($files['profile_picture'])){
		$data["error"] = 1;
		$data["files"] = $files;
		$data["description"] = "Ha ocurrido un error con la imagen recibida";
		$http_response = 500;
	}else{
		$newFile = $files['profile_picture'];
		if($newFile->getError()===UPLOAD_ERR_OK)
		{
			$uploadFileName = $newFile->getClientFilename();
			$array_name = explode(".", $uploadFileName);
			$extension = end($array_name);
			$name_image = $args['id_user'].'.'.$extension;
			$newFile->moveTo( __DIR__.'/../public/assets/profile_picture/'.$name_image);
			$image_user = setImageUser($args['id_user'], $this->db, $name_image);
			if($image_user != -1){
				$data["error"] 			= 0;
				$data["description"] 	= "Imagen de perfil actualizada con éxito";
				$data["path"] 			= $image_user;
			}else{
				$data["error"] = 1;
				$data["description"] = "Ha ocurrido un error al intentar actualizar la información del usuario en la base de datos";
				$http_response = 500;
			}
		}else{
			$data["error"] = 1;
			$data["description"] = "Ha ocurrido un error al intentar subir la imagen";
			$http_response = 500;
		}
	}
	return $this->response->withJson($data, $http_response);
});


$app->delete('/user/[{id}]', function($request, $response, $args){

	$data = array();
	$http_response = 200;
	$sth = $this->db->prepare("DELETE FROM user WHERE id_user=:id");
	try{
		$sth->bindParam("id", $args['id']);
		$sth->execute();
		$data["error"] = 0;
		$data["description"] = "Usuario eliminado con éxito";
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}

	return $this->response->withJson($data, $http_response);
});


function checkEmail($email, $db){

	$sql = "SELECT count(*) as count FROM user WHERE email=:email";
	try{
		$sth = $db->prepare($sql);
		$sth->bindParam("email", $email);
		$sth->execute();
		$c = $sth->fetchObject();
		return intval($c->count);
	}catch(PDOException $e){
		return -1;
	}
}

function setImageUser($id_user, $db, $name_image){

	$path = '/assets/profile_picture/'.$name_image;
	$sql = "UPDATE user SET image=:image WHERE id_user=:id_user";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_user", $id_user);
		$sth->bindParam("image", $path);
		$sth->execute();
		return $path;
	}catch(PDOException $e){
		return -1;
	}

}

function createNewPaymentTrial($id_user, $db){

	$sql = "INSERT INTO payment (`date`, planification_count, resource_count, trial, id_user) VALUES (:fecha, :planification_count, :resource_count, :trial, :id_user)";
	$sth = $db->prepare($sql);
	$planification_count = 2;
	$resource_count 	 = 5;
	$trial 				 = 0;
	try {
		$sth->bindParam("id_user", $id_user);
		$sth->bindParam("planification_count", $planification_count);
		$sth->bindParam("resource_count", $resource_count);
		$sth->bindParam("trial", $trial);
		$fecha = date('Y-m-d');
		$sth->bindParam("fecha", $fecha);
		$sth->execute();
		return 1;
	} catch (PDOException $e) {
		return -1;
	}
}
