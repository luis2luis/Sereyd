<?php


$app->post('/group/', function ($request, $response) {

	$data = array();
	$http_response = 200;
	$input = $request->getParsedBody();
	$sql = "INSERT INTO `group` (school, grade, group_number, period, id_user) VALUES (:school, :grade, :group_number, :period, :user)";
	$sth = $this->db->prepare($sql);
	try{
		$sth->bindParam("school", $input['school']);
		$sth->bindParam("grade", $input['grade']);
		$sth->bindParam("group_number", $input['group']);
		$sth->bindParam("period", $input['period']);
		$sth->bindParam("user", $input['user'], PDO::PARAM_INT);
		$sth->execute();
		$input['id_group'] = $this->db->lastInsertId();
		if($input['id_group'] != false){
			$data["error"] = 0;
			$data["description"] = "Grupo creado con éxito";
			$data["group"] = $input;
		}else{
			$data["error"] = 1;
			$http_response = 500;
			$data["description"] = "Error al crear un grupo nuevo";
		}
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response);

});

$app->get('/group/[{id_group}]', function ($request, $response, $args) {
	
	$data;
	$sth = $this->db->prepare("SELECT id_group, school, grade, group_number, period FROM `group` WHERE id_group=:id_group");
	try{
		$sth->bindParam("id_group", $args['id_group']);
		$sth->execute();
		$group = $sth->fetchObject();

		$total = getTotalAttendance($args['id_group'], $this->db);
		$positive = getPositiveAttendance($args['id_group'], $this->db);

		$group->attendance_percent = ($positive*100)/$total;

		$data = array(

			'error' => 0,
			'data' => array(
				'group' => $group,
				'students' => getStudents($args['id_group'], $this->db),
				'homeworks' => getHomeworks($args['id_group'], $this->db),
				'cooperations' => getCooperations($args['id_group'], $this->db),
				'notices' => getNotices($args['id_group'], $this->db)
			)
		);
	}catch(PDOException $e){
		$data["error"] = 1;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data); 
});

$app->get('/group/information/[{id_group}]', function($request, $response, $args){

});


$app->get('/group/user/[{id_user}]', function ($request, $response, $args) {
	
	$data = array();
	$http_response = 200;
	$sth = $this->db->prepare("SELECT id_group, school, grade, group_number, period FROM `group` WHERE id_user=:id_user");
	try{
		$sth->bindParam("id_user", $args['id_user']);
		$sth->execute();
		$groups = $sth->fetchAll();
		$groups_full = array();	
		foreach ($groups as $k) {

			$total = getTotalAttendance($k['id_group'], $this->db);
			$positive = getPositiveAttendance($k['id_group'], $this->db);
			$k['attendance_percent'] = ($positive*100)/$total;
			$group = array(
				'group' => $k,
				'students' => getStudents($k['id_group'], $this->db),
				'homeworks' => getHomeworks($k['id_group'], $this->db),
				'notices' => getNotices($k['id_group'], $this->db)
			);
			array_push($groups_full, $group);
			$group = array();
		}
		$data = array(
			'error' => 0,
			'data' => $groups_full
		);
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response); 
});

$app->put('/group/[{id_group}]', function($request, $response, $args){

	$data = array();
	$input = $request->getParsedBody();
	$http_response = 200;
	$sql = "UPDATE `group` SET  school=:school, grade=:grade, group_number=:group_number, period=:period WHERE id_group=:id_group";
	$sth = $this->db->prepare($sql);
	try{
		$sth->bindParam("school", $input['school']);
		$sth->bindParam("grade", $input['grade']);
		$sth->bindParam("group_number", $input['group']);
		$sth->bindParam("period", $input['period']);
		$sth->bindParam("id_group", $args['id_group']);
		$count = groupExists($args['id_group'], $this->db);
		if($count["error"]==0){
			if($count["group_contador"]==0){ //no existe el grupo
				$data["error"] = 1;
				$http_response = 500;
				$data["description"] = "No existe un grupo con el identificador: ".$args["id_group"];
			}else{
				$group = $sth->execute();
				$data["error"] = 0;
				$data["description"] = "Grupo actualizado con éxito ";
				$data["group"] = $group;
			}
		}else{
			$data["error"] = 1;
			$http_response = 500;
			$data["description"] = $count["description"];
		}
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	return $this->response->withJson($data, $http_response); 
});


$app->delete('/group/[{id_group}]', function($request, $response, $args){
	
	$data = array();
	$http_response = 200;
	$sth = $this->db->prepare("DELETE FROM `group` WHERE id_group=:id_group");

	try{
		deleteStudentsFromGroup($args['id_group'], $this->db);
		deleteHomeworksFromGroup($args['id_group'], $this->db);
		deleteNoticesFromGroup($args['id_group'], $this->db);
		deleteAllDataFromGroup($args['id_group'], $this->db);
		$sth->bindParam("id_group", $args['id_group']);
		$sth->execute();
		$data["error"] = 0;
		$data["description"] = "Grupo eliminado con éxito";
		$data["id_group"] = $args['id_group'];
	}catch(PDOException $e){
		$data["error"] = 1;
		$http_response = 500;
		$data["description"] = $e->getMessage();
	}
	
	return $this->response->withJson($data, $http_response);
});

function groupExists($id_group, $db){

	$r = array();
	$sql = "SELECT count(*) as contador FROM `group` WHERE id_group=:id_group";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_group", $id_group);
		$sth->execute();
		$c = $sth->fetchObject();
		$r["error"] = 0;
		$r["group_contador"] = $c->contador;
		return $r;
	}catch(PDOException $e){
		$r["error"] = 1;
		$r["description"] = $e->getMessage();
		return $r;
	}

}

function deleteStudentsFromGroup($id_group, $db){

	$sth = $db->prepare("DELETE FROM `student` WHERE id_group=:id_group");
	try{
		$sth->bindParam("id_group", $id_group);
		$sth->execute();
	}catch(PDOException $e){
		
	}
}

function deleteHomeworksFromGroup($id_group, $db){

	$sth = $db->prepare("DELETE FROM `homework` WHERE id_group=:id_group");
	try{
		$sth->bindParam("id_group", $id_group);
		$sth->execute();
	}catch(PDOException $e){
		
	}
}

function deleteNoticesFromGroup($id_group, $db){

	$sth = $db->prepare("DELETE FROM `notice` WHERE id_group=:id_group");
	try{
		$sth->bindParam("id_group", $id_group);
		$sth->execute();
	}catch(PDOException $e){
		
	}
}

function getHomeworks($id_group, $db){
	
	$sql = "SELECT * FROM `homework` WHERE id_group=:id_group";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_group", $id_group);
		$sth->execute();
		return $sth->fetchAll();
	}catch(PDOException $e){
		return "Error obteniendo las tareas";
	}
}


function getStudents($id_group, $db){
	
	$sql = "SELECT * FROM `student` WHERE id_group=:id_group";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_group", $id_group);
		$sth->execute();
		return $sth->fetchAll();
	}catch(PDOException $e){
		return "Error obteniendo los estudiantes";
	}
}

function getNotices($id_group, $db){
	
	$sql = "SELECT * FROM `notice` WHERE id_group=:id_group";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_group", $id_group);
		$sth->execute();
		return $sth->fetchAll();
	}catch(PDOException $e){
		return "Error obteniendo los avisos";
	}
}

function getCooperations($id_group, $db){
	
	$sql = "SELECT * FROM `cooperation` WHERE id_group=:id_group";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_group", $id_group);
		$sth->execute();
		return $sth->fetchAll();
	}catch(PDOException $e){
		return "Error obteniendo los avisos";
	}
}

function getTotalAttendance($id_group, $db){


	$sql = "SELECT COUNT(*) AS count_total    FROM `attendance` WHERE id_group=:id_group";
	
	try{
		$sth = $db->prepare($sql);
		$sth->bindParam("id_group", $id_group);
		$sth->execute();
		$count_total = $sth->fetchObject();
		return $count_total->count_total;
	}catch(PDOException $e){
		return "Error obteniendo los valores";
	}

}

function getPositiveAttendance($id_group, $db){

	$sql = "SELECT COUNT(*) AS count_positive FROM `attendance` WHERE id_group=:id_group AND value=1";
	
	try{
		$sth = $db->prepare($sql);
		$sth->bindParam("id_group", $id_group);
		$sth->execute();
		$count_positive = $sth->fetchObject();
		return $count_positive->count_positive;
	}catch(PDOException $e){
		return "Error obteniendo los valores";
	}

}

function deleteAllDataFromGroup($id_group, $db){
	$sql = "DELETE FROM student WHERE id_group=:id_group;DELETE FROM cooperation WHERE id_group=:id_group;DELETE FROM homework WHERE id_group=:id_group;DELETE FROM notice WHERE id_group=:id_group;DELETE FROM group_diagnostic WHERE id_group=:id_group;DELETE FROM group_evaluation WHERE id_group=:id_group;";
	$sth = $db->prepare($sql);
	try{
		$sth->bindParam("id_group", $id_group);
		$band = $sth->execute();
		return $band;
	}catch(PDOException $e){
		return -1;
	}
}