<?php


$app->get('/diary/[{id_user}]', function ($request, $response, $args) {

  $id_user        = $args["id_user"];
  $http_response  = 200;
  $data           = array();
  $sql            = "SELECT id_diary, title, description, date_creation FROM diary WHERE id_user=:id_user";
  $sth            = $this->db->prepare($sql);
  try {
    $sth->bindParam("id_user", $id_user);
  	$sth->execute();
  	$diary = $sth->fetchAll();
    $data["error"]       = 0;
    $data["description"] = "éxito al obtener el diario de la educadora";
    $data["entries"]     = $diary;
  } catch (PDOException $e) {
    $data["error"]        = 1;
    $http_response        = 500;
    $data["description"]  = "error al obtener el diaro de la educadora";
  }
	return $this->response->withJson($data, $http_response);

});


$app->post('/diary', function ($request, $response, $args) {

  $input 					= $request->getParsedBody();
  $title          = $input["title"];
  $description    = $input["description"];
  $id_user        = $input["id_user"];
  $date           = date('Y-m-d');
  $http_response 	= 200;
  $sql 						= "INSERT INTO diary (title, description, date_creation, id_user) VALUES (:title, :description, :date_creation, :id_user)";

  try {
    $sth = $this->db->prepare($sql);
    $sth->bindParam("title", $title);
    $sth->bindParam("description", $description);
    $sth->bindParam("date_creation", $date);
    $sth->bindParam("id_user", $id_user);
    $sth->execute();
    $input["id_diary"]      = $this->db->lastInsertId();
    $input["date_creation"] = $date;
    if ($input["id_diary"] != false) {
      $data["error"] = 0;
      $data["description"] = "Entrada de diario creada con éxito";
      $data["diary"] = $input;
    }else{
      $data["error"]       = 1;
      $http_response 	     = 500;
      $data["description"] = "Error al crear entrada de diario";
    }
  } catch (PDOException $e) {
    $data["error"]        = 1;
    $http_response 	      = 500;
    $data["description"]  = $e->getMessage();
  }
  return $this->response->withJson($data, $http_response);

});
