/*
-- Query: SELECT * FROM soyeduca_sereyd.resource
LIMIT 0, 1000

-- Date: 2017-08-11 11:48
*/
INSERT INTO `resource` (`id_resource`,`name`,`url`,`id_category_resource`) VALUES (1,'Recurso de Prueba 1','/assets/resources/test.pdf',1);
INSERT INTO `resource` (`id_resource`,`name`,`url`,`id_category_resource`) VALUES (2,'Recurso de Prueba 2','/assets/resources/test.pdf',1);
INSERT INTO `resource` (`id_resource`,`name`,`url`,`id_category_resource`) VALUES (3,'Recurso de Prueba 3','/assets/resources/test.pdf',1);
INSERT INTO `resource` (`id_resource`,`name`,`url`,`id_category_resource`) VALUES (4,'Recurso de Prueba 4','/assets/resources/test.pdf',2);
INSERT INTO `resource` (`id_resource`,`name`,`url`,`id_category_resource`) VALUES (5,'Recurso de Prueba 5','/assets/resources/test.pdf',2);
INSERT INTO `resource` (`id_resource`,`name`,`url`,`id_category_resource`) VALUES (6,'Recurso de Prueba 6','/assets/resources/test.pdf',2);
INSERT INTO `resource` (`id_resource`,`name`,`url`,`id_category_resource`) VALUES (7,'Recurso de Prueba 7','/assets/resources/test.pdf',3);
INSERT INTO `resource` (`id_resource`,`name`,`url`,`id_category_resource`) VALUES (8,'Recurso de Prueba 8','/assets/resources/test.pdf',3);
INSERT INTO `resource` (`id_resource`,`name`,`url`,`id_category_resource`) VALUES (9,'Recurso de Prueba 9','/assets/resources/test.pdf',3);
INSERT INTO `resource` (`id_resource`,`name`,`url`,`id_category_resource`) VALUES (10,'Recurso de Prueba 10','/assets/resources/test.pdf',4);
INSERT INTO `resource` (`id_resource`,`name`,`url`,`id_category_resource`) VALUES (11,'Recurso de Prueba 11','/assets/resources/test.pdf',4);
INSERT INTO `resource` (`id_resource`,`name`,`url`,`id_category_resource`) VALUES (12,'Recurso de Prueba 12','/assets/resources/test.pdf',4);
INSERT INTO `resource` (`id_resource`,`name`,`url`,`id_category_resource`) VALUES (13,'Recurso de Prueba 13','/assets/resources/test.pdf',4);
