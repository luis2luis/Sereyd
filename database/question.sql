INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (1,'¿Se expresa con madurez de acuerdo a su edad? \r¿Se expresa con madurez de acuerdo a su edad?',1);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (2,'Se comunica con otros niños y adultos dentro y fuera del aula',1);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (3,'Mantiene la atención y sigue una conversación',1);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (4,'Describe personas, objetos',1);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (5,'Escucha y cuenta relatos de lugares y fenómenos que conoce',1);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (6,'¿Muestra seguridad al hablar? ',1);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (7,'Manifiesta fluidez en su expresión oral',1);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (8,'Escucha lo que otros dicen',1);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (9,'Identifica algunos portadores de texto',1);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (10,'Comprende los conceptos de \"igual\" y \"diferente\"',1);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (11,'Habla en oraciones de cinco a seis palabras',1);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (12,'Habla lo suficientemente claro como para que lo comprendan los extraños',1);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (13,'Cuenta historias',1);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (14,'Usa \"yo\" y \"usted\" apropiadamente',1);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (15,'Usa oraciones de más de tres palabras',1);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (16,'Nombra correctamente algunos colores',2);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (17,'Comprende el concepto de contar y puede conocer algunos números',2);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (18,'Empieza a tener un sentido claro del tiempo',2);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (19,'Sigue órdenes de tres partes',2);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (20,'Comprende el concepto de igual/diferente',2);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (21,'Puede copiar un círculo',2);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (22,'Identifica los colores primarios y algunos secundarios',2);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (23,'Distingue entre objetos grandes y pequeños, pesados y livianos',2);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (24,'Hace clasificación por 1 atributo.',2);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (25,'Distingue con objetos concretos los cuantificadores: muchos, pocos, todos, ninguno',2);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (26,'Sigue la secuencia o patrón (tamaño, color), que se le da con bloques o cuentas',2);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (27,'Cuenta hasta 10 imitando al adulto, pero no hace correspondencia',2);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (28,'Identifica y nombre objetos que son iguales y/o diferentes',2);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (29,'Identifica por lo menos 3 figuras geométricas (círculo, cuadrado y triángulo)',2);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (30,'Separa objetos por categorías',2);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (31,'',3);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (32,'Observa con atención y formula preguntas',3);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (33,'Reconocer entre lo natural y lo no natural',3);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (34,'Reconoce entre lo vivo y lo no vivo',3);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (35,'Sabe diferenciar entre plantas y animales',3);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (36,'Sabe lo que es limpio y sucio',3);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (37,'Reconoce y respeta diferentes formas de vida',3);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (38,'Conoce algunos valores como: Respeto, Patriotismo, Honestidad, Igualdad, Solidaridad, Amistad',3);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (39,'Reconoce algunas características de su propia cultura y otras',3);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (40,'Respeta a los demás',3);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (41,'Respeta Normas de convivencia',3);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (42,'Describe características de los seres vivos',3);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (43,'Intenta explicar lo que sabe del medio natural',3);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (44,'Expresa curiosidad por conocer más de los seres vivos y del medio natural',3);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (45,'Reconoce algunos fenómenos naturales',3);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (46,'¿Sabe seguir instrucciones?',3);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (47,'Formula preguntas precisas y da respuestas coherentes',7);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (48,'Da información amplia de sí mismo',7);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (49,'Proporciona información de su familia (nombres, parentescos, qué hacen)',7);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (50,'Conversa y dialoga con alternancia en intervenciones y su intercambio verbal es preciso',7);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (51,'Explica sobre el conocimiento que tiene acerca de algo en particular.',7);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (52,'Recuerda y explica por pasos las actividades que ha realizado',7);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (53,'Identifica la acción de leer y sabe que las marcas gráficas (letras) dicen algo',7);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (54,'Tiene ideas sobre las funciones del lenguaje escrito (contar o narrar (cuentos), enviar mensajes (escribir), anunciar sucesos (hacer notas o letreros), etc.)',7);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (55,'Intenta representar sus ideas por medio de diversas formas (dibujos y marcas parecidas a las letras)',7);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (56,'Habla sobre lo que anota y lo que cree que está escrito',7);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (57,'Identifica donde hay: una imagen (dibujo), donde letras y donde números',7);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (58,'Escribe su nombre',7);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (59,'Habla de sus experiencias',7);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (60,'Escucha lo que otros dicen',7);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (61,'Su vocabulario es amplio',7);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (62,'Establece relaciones de igualdad y desigualdad, agregar y quitar, distingue grande y pequeño',8);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (63,'Cuenta estableciendo correspondencia uno a uno entre el objeto y el número',8);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (64,'Cuenta elementos de una colección y la representa numéricamente',8);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (65,'Identifica que los números sirven para contar y conoce hasta el número:',8);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (66,'Reconoce algunos usos de los números en la vida cotidiana (playeras, casas, autos, precios, etc.)',8);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (67,'Identifica y dice la secuencia de la serie numérica del 1.- al  10.- oralmente',8);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (68,'Reconoce y reproduce figuras geométricas básicas',8);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (69,'Es capaz de reconocer y nombrar objetos con cualidades geométricas (forma, tamaño, núm. de lados)',8);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (70,'Establece relaciones de orientación (al lado de, debajo, sobre, arriba, delante, atrás, izquierda, derecha',8);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (71,'Establece relaciones de proximidad cerca de, lejos de,…',8);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (72,'Establece relaciones de interioridad, dentro de, fuera de,…',8);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (73,'Identifica que los objetos y personas se pueden medir (menciona algunos instrumentos)',8);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (74,'Conoce los días de la semana.',8);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (75,'Conoce los nombres de algunos meses.',8);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (76,'Distingue entre el ayer, hoy y mañana.',8);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (77,'Reconoce algunos fenómenos naturales y dice su nombre',9);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (78,'Reconoce la diferencia entre seres vivos y seres no vivos',9);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (79,'Reconoce entre las plantas y animales',9);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (80,'Sabe del cuidado y protección del medio natural',9);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (81,'Hace inferencias sobre la información que ya posee',9);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (82,'Reconoce el papel que desempeña cada integrante de su familia y lo menciona',9);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (83,'Hace descripciones con detalles de lo que observa',9);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (84,'Hace comparaciones entre dos elementos',9);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (85,'Refiere diferentes hechos de su historia personal y familiar',9);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (86,'Sabe que son sus derechos y menciona algunos',9);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (87,'Sabe que son las obligaciones y menciona algunas',9);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (88,'Reconoce de la existencia de normas para la convivencia en grupo y menciona algunas',9);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (89,'Reconoce su entorno social y natural inmediato',9);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (90,'Nombra fenómenos naturales que conoce.',9);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (91,'Respeta normas de convivencia',9);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (92,'Es capaz de coordinar los movimientos de su cuerpo',10);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (93,'Mantiene el equilibrio',10);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (94,'Camina, corre, trepa, se arrastra',10);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (95,'Maneja con cierta destreza diversos objetos o piezas para construir, armar, ensamblar, etc.',10);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (96,'Representa y crea imágenes o símbolos (con lápiz, pintura, dedo, en papel, tierra, etc.',10);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (97,'Comprende las funciones que tienen algunas partes de su cuerpo y las menciona',10);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (98,'Identifica algunas enfermedades comunes de la edad',10);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (99,'Reconoce algunas medidas para su cuidado y salud personal',10);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (100,'Reconoce comidas sanas y las diferencia de las comidas chatarra',10);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (101,'Aplica medidas de higiene, alimentación y cuidado personal (loncheras, cosas personales)',10);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (102,'Comprende sobre el cuidado de su integridad física ',10);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (103,'Habla acerca de las personas que le generan confianza, seguridad y afecto y por que',10);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (104,'¿Tiene hábitos de higiene personal?',10);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (105,'¿Tiene conciencia del cuidado de la salud y de sí mismo?',10);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (106,'¿Diferencia comida chatarra de comida sana?',10);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (107,'Reconoce de su cuerpo: su imagen, rasgos físicos y características',11);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (108,'Reconoce de su cuerpo sus alcances y limitaciones',11);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (109,'Identifica los géneros masculino y femenino e identifica a cuál pertenece',11);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (110,'Identifica algunas características de cada uno de los géneros (masculino y femenino)',11);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (111,'Explora y hace comparaciones para encontrar semejanzas en los géneros M y F',11);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (112,'Reconoce que hay reglas establecidas y menciona algunas de ellas',11);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (113,'Acepta y participa en actividades y juegos conforme a las reglas establecidas',11);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (114,'Identifica estados emocionales tales como ira, vergüenza, tristeza, felicidad, temor, etc…',11);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (115,'Su forma de participación y colaboración en el grupo es autónoma',11);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (116,'Se involucra en actividades colectivas',11);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (117,'Se reconoce parte de un grupo y establece lazos de amistad',11);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (118,'Comprende que en los juegos hay reglas y acepta cuando gana o pierde',11);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (119,'Respeta las reglas de la escuela y el aula.',11);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (120,'Demuestra seguridad en sí mismo.',11);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (121,'Se relaciona con otros niños.',11);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (122,'Es capaz de crear producciones propias',12);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (123,'Ilumina manejando diferentes contrastes de colores',12);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (124,'Elige y usa objetos adecuados para realizar una tarea asignada (tijeras, crayolas, pinceles, etc.)',12);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (125,'Es autónomo al elegir y tomar decisiones de qué materiales usar',12);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (126,'Es capaz de controlar la autorregulación de los movimientos de su cuerpo',12);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (127,'Sigue el ritmo de la música utilizando las palmas, pies e instrumentos',12);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (128,'Escucha y canta canciones',12);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (129,'Describe lo que imagina al escuchar una melodía',12);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (130,'Representa mediante la expresión corporal animales, objetos, personajes, etc.',12);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (131,'Comunica emociones y sentimientos a través de gestos, acciones y palabras',12);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (132,'Se involucra en juegos espontáneos e imaginarios',12);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (133,'Pone atención a las indicaciones y las ejecuta',12);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (134,'Sabe seguir el ritmo de una canción con el cuerpo.',12);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (135,'Se mueve con soltura al escuchar música',12);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (136,'Experimenta mediante técnicas, materiales y herramientas',12);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (137,'Su vocabulario le permite comunicarse',13);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (138,'Sabe su nombre y lo dice',13);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (139,'Dice el nombre de su papá y mamá',13);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (140,'Conversa y dialoga con sus pares',13);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (141,'Platica lo que hace en casa',13);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (142,'Dice cuál es su juguete y juego preferidos',13);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (143,'Pone atención cuando le leen un cuento',13);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (144,'Llora a la hora de entrada',13);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (145,'Pone atención cuando le hablan',13);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (146,'Crea mundos imaginarios',13);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (147,'Se dirige al adulto a preguntar o solicitar algo',13);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (148,'Su vocabulario es reducido',13);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (149,'Hace intentos de escritura a través de dibujos',13);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (150,'Identifica su nombre',13);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (151,'Logra conversar y dialogar claramente',13);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (152,'Separa objetos por semejanza (tamaño, color, forma) sin saber el por que',14);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (153,'Identifica donde hay más y donde hay menos',14);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (154,'Distingue entre grande y pequeño',14);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (155,'Dice los números del 1 al 5 aleatoriamente',14);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (156,'Ubica arriba y debajo de...',14);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (157,'Identifica delante y atrás',14);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (158,'Identifica dentro y fuera',14);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (159,'Identifica cerca y lejos',14);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (160,'Reparte dulces, juguetes, material, etc.',14);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (161,'Menciona un número “x” para decir cuántos objetos tiene',14);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (162,'Tiene conocimiento de lo que es un número',14);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (163,'Manipula el material concreto libremente',14);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (164,'Identifica entre plantas y animales',15);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (165,'Sabe que hay seres vivos y seres no vivos',15);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (166,'Sabe cuándo es de día y de noche',15);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (167,'Identifica algunos fenómenos naturales (lluvia, viento, calor, calor, etc.)',15);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (168,'Sabe del cuidado y protección del medio natural',15);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (169,'Sabe decir lo que hace su papá y su mamá',15);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (170,'Habla de sus pertenencias',15);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (171,'Hace descripciones de lo que observa (de dos a cuatro detalles)',15);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (172,'Hace comparaciones entre dos elementos diferentes',15);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (173,'Habla sobre algunos acontecimientos de historia personal y familiar',15);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (174,'Identifica que es una obligación',15);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (175,'Identifica que son las reglas de comportamiento',15);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (176,'Es capaz de hacer movimientos con su cuerpo',16);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (177,'Mantiene el equilibrio',16);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (178,'Corre, sube, gatea, se arrastra',16);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (179,'Manipula objetos concretos para construir y armar libremente',16);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (180,'Sus producciones se asemejan a formas concretas',16);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (181,'Identifica algunas partes de su cuerpo',16);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (182,'Identifica algunas enfermedades comunes de la edad',16);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (183,'Sabe decir de algunos cuidados para la salud de su cuerpo',16);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (184,'Identifica la comida sana y la chatarra',16);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (185,'Maneja medidas de higiene, alimentación y cuidado de sus pertenencias (lonchera, etc.)',16);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (186,'Sabe identificar sobre el cuidado de su integridad física',16);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (187,'Sabe acerca de las personas que le generan confianza y seguridad',16);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (188,'¿Tiene hábitos de cuidado personal?',16);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (189,'¿Conoce la diferencia de limpio y sucio?',16);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (190,'¿Está desarrollada su Coordinación motriz fina?',16);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (191,'Tiene conocimiento de las principales partes de su cuerpo y las menciona',17);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (192,'Identifica algunos rasgos físicos',17);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (193,'Identifica un niño de una niña',17);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (194,'Se ubica al sexo que pertenece',17);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (195,'Identifica semejanzas del sexo masculino y femenino',17);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (196,'Identifica diferencias del sexo masculino y femenino',17);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (197,'Obedece y ejecuta las indicaciones',17);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (198,'Se involucra con facilidad en actividades colectivas',17);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (199,'Interactúa con sus pares espontáneamente',17);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (200,'Respeta reglas establecidas',17);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (201,'Identifica estados emocionales (miedo, tristeza, enojo, felicidad)',17);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (202,'Manifiesta verbalmente su estado emocional',17);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (203,'Es autónomo e independiente.',17);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (204,'Presta atención al realizar sus actividades.',17);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (205,'Trabaja en equipo.',17);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (206,'Sus trazos se asemejan a líneas y/o garabatos',18);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (207,'Sus trazos se asemejan a formas más concretas',18);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (208,'Es capaz de tomar adecuadamente la crayola',18);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (209,'Al iluminar respeta los límites de la imagen',18);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (210,'Utiliza un solo tono de color al iluminar',18);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (211,'Es capaz de desplazarse con su cuerpo según las indicaciones',18);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (212,'Es capaz de asumir un rol diferente al suyo',18);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (213,'Es capaz de seguir el ritmo de una melodía con sus palmas',18);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (214,'Mueve su cuerpo al ritmo de la música',18);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (215,'Comunica emociones y sentimientos a través de gestos y acciones',18);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (216,'Se involucra en juegos espontáneos e imaginarios',18);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (217,'Pone atención a las indicaciones y las ejecuta',18);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (218,'Observa e interpreta imágenes de sus compañeros',18);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (219,'Representa situaciones reales e imaginarias',18);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (220,'Canta e inventa canciones',18);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (222,'Salta con ambos pies',4);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (223,'Mantiene el equilibrio',4);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (224,'Alterna movimientos (con manos y pies)',4);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (225,'Lanza y patea la pelota',4);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (226,'Respeta contornos',4);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (227,'Se viste solo',4);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (228,'Pasa las hojas de un libro',4);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (229,'Tiene hábitos de higiene personal',4);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (230,'¿Se sabe peinar y asear?',4);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (231,'Salta y se para sobre un pie hasta por cinco segundos',4);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (232,'Sube y baja escaleras sin apoyo',4);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (233,'Patea una pelota hacia adelante',4);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (234,'Se mueve hacia adelante y atrás con agilidad',4);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (235,'Dibuja una persona con dos a cuatro partes del cuerpo',4);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (236,'Añade una pierna y/o un brazo a una figura incompleta de una persona',4);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (237,'Demuestra seguridad de sí mismo ',5);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (238,'Reconoce cualidades y capacidades de las personas',5);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (239,'Comprende que hay normas y reglas para una mejor convivencia',5);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (240,'Se relaciona con otros niños ',5);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (241,'Respeta las reglas de la escuela y el aula ',5);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (242,'Expresa necesidades y deseos',5);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (243,'¿Mantiene la atención cuando se le habla?',5);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (244,'¿Muestra disposición para involucrarse en las actividades?',5);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (245,'¿Se integra con sus compañeros de grupo?',5);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (246,'Ayuda a sus compañeros',5);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (247,'Colabora con otros niños',5);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (248,'Juega a mamá o papá',5);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (249,'Negocia las soluciones para los conflictos',5);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (250,'Arremete contra todos sin autocontrol cuando está enojado o molesto',5);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (251,'Habla de lo que le gusta o no le gusta en su casa',5);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (252,'Canta e inventa canciones ',6);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (253,'Imita movimientos y sonidos ',6);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (254,'Expresa sentimientos a través de la música',6);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (255,'Se mueve con soltura al escuchar la música',6);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (256,'Participa en actividades de cantos y juegos',6);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (257,'Expresa sentimientos a través de la danza',6);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (258,'Experimenta mediante técnicas, materiales y herramientas',6);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (259,'Se expresa creativamente a través de los diversos materiales',6);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (260,'Aprecia y compara sus trabajos',6);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (261,'Comunica creativamente sus ideas y sentimientos mediante representaciones plásticas',6);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (262,'Representa personajes reales e imaginarios mediante el juego',6);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (263,'Describe (características de los personajes)',6);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (264,'Representa situaciones reales e imaginarias',6);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (265,'Imita movimientos y sonidos',6);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (266,'Recuerda la melodía de las canciones conocidas',6);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (267,'Pregunta de Prueba1',19);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (268,'Pregunta de Prueba 2',19);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (269,'Pregunta de Prueba 3',20);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (270,'Pregunta de Prueba 4',20);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (271,'Pregunta de Prueba 5',21);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (272,'Pregunta de Prueba 6',21);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (273,'Pregunta de Prueba 7',22);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (274,'Pregunta de Prueba 8',22);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (275,'Pregunta de Prueba 9',23);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (276,'Pregunta de Prueba 10',23);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (277,'Pregunta de Prueba 11',24);
INSERT INTO `question` (`id_question`,`question`,`id_test`) VALUES (278,'Pregunta de Prueba 12',24);
